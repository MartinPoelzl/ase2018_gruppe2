
package at.campus02.unoserver;

import java.io.Serializable;
import java.util.Arrays;

/**
 *<em> Erstellung der Karte mit Funktions-Nummer und Farbe, vergleichen, Punkte,..</em>
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika Strau&szlig;<br>
 * 
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
public class Card implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7936467697140694819L;
	public char cardColor;
	public int number;
	
/** Konstroktor f&uuml;r Karten (Farbe, interne Nr.)
 * 
 * @param color setzt Farbe der Karte
 * @param number weist der Karte eine interne Nummer zu &uuml;ber die die Art der Karte spezifiziert wird
 */
	public Card(char color, Integer number)
	{
		this.cardColor = color;
		this.number = number;
		
	}
	
	/**
	 * Vergleicht mit Handkarten nach der internen Nummer und Farbe
	 * @param cardOnHand Karte auf der Hand
	 * @return gibt bei &Uuml;bereinstimmung true zur&uuml;ck
	 */
	public boolean match(Card cardOnHand)
	{	
		if(this.number == cardOnHand.number)
		{
			return true;
		}
		else if(this.cardColor == cardOnHand.cardColor)
		{
			return true;
		}
		else if(this.number != 10 && cardOnHand.number == 11)
		{
			return true;
		}
		else if(this.number != 11 && cardOnHand.number == 10)
		{
			return true;
		}
		
		else
		{
			return false;			
		}
	}

	/**
	 * Pr&uuml;ft ob es sich um die SELBE Karte handelt
	 * @param cardOnHand Karte in der Hand
	 * @return false, bei Nicht&uuml;bereinstimmung
	 */
	public boolean isSameCard(Card cardOnHand)
	{
		if(this.number == cardOnHand.number && this.cardColor == cardOnHand.cardColor)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Setzt die Farbe der Karte
	 * @param cardColor Kartenfarbe (Gr&uuml;n, Blau; Rot, Gelb)
	 */
	protected void setCardColor(char cardColor) 
	{
		this.cardColor = cardColor;
	}

	public boolean isActionCard()
	{
		if(this.number < 10)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	/**
	 * Punkte f&uuml;r die Wertung der Sonderkarten werden zugewiesen 
	 * @return gibt die Punkte f&uuml;r die Datenbank (50: WldDr und Wld, 20: Skp, Rev und +2 sonst: 0)
	 */
	
	public int getCardPoints() {
		
		if (this.number < 10)
		{
			return number;
		} else if (this.number == 10 || this.number == 11) // WldDr & Wld
		{
			return 50;
		} 
		else if (this.number == 12 || this.number == 13 || this.number == 14) // Skp, Rev und +2
		{
			return 20;
		} 
		else
		{
			System.out.println("Error - getCardPoints.");
			return 0;
		}
		
	}


	/** 
	* Gibt die Karte als String nebeneinander auf die Konsole aus.
	* 
	* */
	public String toString()
	{ 
		if (this.number < 10)
		{
			return String.format(" %c %d ", cardColor, number);
		} else if (this.number == 10)  // WldDr & Wld String action Initialfarbe farbe='P',
		{
			return String.format("WldDr");
		} else if (this.number == 11)
		{
			return String.format(" Wld ");
		} 
		else if (this.number == 12)  // Farbigen Aktionskarten zb Blau +2
		{
			return String.format("%c +2 ", cardColor);
		} 
		else if (this.number == 13)  // Farbigen Aktionskarten zb Blau +2
		{
			return String.format("%c Skp", cardColor);
		} 
		else if (this.number == 14)  // Farbigen Aktionskarten zb Blau +2
		{
			return String.format("%c Rev", cardColor);
		} 
		else
		{
			return String.format("Fehler bei der Karte");
		}
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cardColor;
		result = prime * result + number;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (cardColor != other.cardColor)
			return false;
		if (number != other.number)
			return false;
		return true;
	}
/**
 * @return Karten-Farbe
 */
	public char getColor()
	{
		return this.cardColor;
	}
	
	/**
	 * @return interne Nummer
	 */
	public int getNumber()
	{
		return this.number;
	}
}
