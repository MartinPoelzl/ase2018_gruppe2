package at.campus02.unoserver;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import at.campus02.messages.ToClientHand;

/** Jeder ClientRunnable startet in einem neuen Thread
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
public class ClientRunnable implements Runnable
{
	private Socket clientSocket;
	private UnoGameManagement gm;
	private Gambler gambler;
	
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;

	ClientRunnable(Socket client, UnoGameManagement gameManager, ObjectInputStream ois, 
			ObjectOutputStream oos, Gambler emptyGambler) 
	{
		super();
		this.clientSocket = client;
		this.gm = gameManager;
		
		this.gambler = emptyGambler;
		
		this.ois = ois;
		this.oos = oos;
	}

	/**
	 * Nimmt Spielernamen entgegen, gibt Spieler Socket sowie IO-Streams
	 */
	@Override
	public void run()
	{
		try 
		{				
			gambler.setName((String) ois.readObject());
			gambler.setSocket(clientSocket);
			gambler.setOis(ois);
			gambler.setOos(oos);
			System.out.println(gambler.getName() + " joined the game.");
			
		
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}

	}

}
