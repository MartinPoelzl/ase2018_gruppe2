package at.campus02.unoserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;
import java.awt.Desktop;
import java.io.IOException;

import at.campus02.database.UNOSqliteDatabase;
import at.campus02.unoserver.*;
/** <em>Enth&auml;lt die eigentliche Anwendung "UnoApp"</em><br>
 * Erstellen des GameManagers, Scanner, Kartendesign
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
public class SinglePlayerApp
{

	/**
	 * Erstellen des GameManagers, &ouml;ffnen des Scanners f&uuml;r die Konsolen-Eingabe<br>
	 * Eingeben des Spielernamens<br>
	 * Hinzuf&uuml;gen von X Bots zum Spielertisch<br>
	 * Austeilen der Karten<br>
	 * Initialisieren der Spielz&uuml;ge/Spielrunde (GameTurn)<br>
	 * @param args -
	 */

	public static void main(String[] args)
	{

		UnoGameManagement mngm = new UnoGameManagement();
		ArrayList<Gambler> botsList;
		UNOSqliteDatabase db = new UNOSqliteDatabase();
		db.createTable();
		int sessionID = db.getSessionID();
		int newSessionID = sessionID + 1;
		
		Scanner entry = null;

		try
		{

			System.out.println("Bitte deinen Spielernamen eingeben.");
			entry = new Scanner(System.in);
			String name = entry.nextLine();
						
			Gambler player = new Gambler(name, entry);
			
			boolean goingOn = true;
			
			int botCtr = 0;
			
			botsList = botNameGenerator();
			
			mngm.addGambler(player);
			
			for(int i = 0; i < 3; i++) 
			{
				if(botCtr == botsList.size())
				{
					botCtr = 0;
					Collections.shuffle(botsList);
				}
				mngm.addGambler(botsList.get(botCtr)); 
				System.out.println(botsList.get(botCtr).getName() + " hat sich der Runde angeschlossen.");
				botCtr++;
			}
			
			db.insertPlayers(newSessionID);
				
			while(goingOn)
			{	
				if(db.getSessionID() != newSessionID) 
				{
					db.insertPlayers(newSessionID);
				}
				
				int roundCounter = 0;
				
				mngm.clearDecks();
				mngm.generateCards();
				mngm.dealOutCards();
				
				while (mngm.gameTurnSinglePayer())
				{
					roundCounter++;
				}
				
				int allPoints = mngm.getPointsFromAllGamblers();
				System.out.println("Spiel endete nach " + roundCounter + " Z�gen. Das entspricht ungef�hr " + roundCounter/4 + " Runden.");
				System.out.println(mngm.getWinner() + " bekommt " + allPoints + " Punkte\r\n");
				db.updateNewScore(mngm.getWinner(), newSessionID, allPoints);
				db.getScoreFromCurrentRound();
				
				mngm.clearDecks();
				mngm.generateCards();
				
				boolean goingOnDecision = true;
				
				System.out.println("\r\nEnter dr�cken f�r n�chste Runde.");
				String announcement = entry.nextLine();
				
				
				if (db.getMaxScoreFromCurrentRound() >= 500)
				{
					System.out.println("\r\nSpieler " + db.getNameOfSessionWinner() + " hat " + db.getMaxScoreFromCurrentRound()
							+ " erreicht und hat die Session gewonnen.");
					newSessionID = db.getSessionID()+1;
					while(goingOnDecision == true)
				{
					
					System.out.println("\r\nNoch ein Spiel? :-)   (Y/N  oder J/N als Eingabe m�glich.)");
					String decision = entry.nextLine();
					if(decision.equalsIgnoreCase("J") || decision.equalsIgnoreCase("Y"))
					{
						goingOnDecision = false;
						goingOn = true;
					}
					else if(decision.equalsIgnoreCase("N"))
					{
						System.out.println("Dann bis zum n�chsten Mal. ^^");
						goingOnDecision = false;
						goingOn = false;
					}
					else
					{
						goingOnDecision = true;
						System.out.println("Fehler bei Ja / Nein Abfrage. Bitte um erneute Entscheidung.");
					}
				}
			}			
		}		
	}finally
		{
			entry.close();
		}

	}
	
	/**
	 * Im File "namen.txt" sind Namen f&uuml;r die Bots gespeichert.
	 * @return Liste von Bots
	 */
	public static ArrayList<Gambler> botNameGenerator()
	{
		 FileReader fr;
		 BufferedReader br; 
		
		ArrayList<String> namen = new ArrayList<>();
		ArrayList<Gambler> botsList = new ArrayList<>();
		
		try {
			fr = new FileReader("namen.txt");
			br = new BufferedReader(fr);
			
			
			String next;
			
			while((next = br.readLine()) != null)
			{
				namen.add(next + " (Bot)");
			}
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		Collections.shuffle(namen);

		for(int i = 0; i < 87; i++)
		{
			if (namen.get(i).charAt(0) == 'A')
			{
				botsList.add(new BotA(namen.get(i)));
			} else if (namen.get(i).charAt(0) == 'B')
			{
				botsList.add(new BotB(namen.get(i)));
			} else if (namen.get(i).charAt(0) == 'C')
			{
				botsList.add(new BotC(namen.get(i)));
			}
		}
		
		return botsList;
	}
	
}