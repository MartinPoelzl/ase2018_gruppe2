package at.campus02.unoserver;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

import at.campus02.database.UNOSqliteDatabase;
import at.campus02.messages.*;

/**
 * <em>Erstellt den Spieler</em><br>
 * Spieler hat Namen, kann Eingaben auf der Konsole machen und isBot=false
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
public class Gambler
{
	protected String name;
	private boolean isBot;
	private Scanner input;
	private ArrayList<Card> handCards = new ArrayList<>();
	private Socket socket;
	private UNOSqliteDatabase db = new UNOSqliteDatabase();

	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	private Comparator<Card> gamblerHandSort = new Comparator<Card>()
	{
		@Override
		public int compare(Card card1, Card card2)
		{
			if (card1.getNumber() == 10 || card1.getNumber() == 11)
			{
				return 1;
			}
			if (card2.getNumber() == 10 || card2.getNumber() == 11)
			{
				return -1;
			}

			if (card1.getColor() > card2.getColor())
			{
				return 1;
			}

			if (card1.getColor() < card2.getColor())
			{
				return -1;
			}

			if (card1.getNumber() < card2.getNumber())
			{
				return -1;
			}
			if (card1.getNumber() > card2.getNumber())
			{
				return 1;
			}

			return 0;
		}
	};

	/**
	 * Hauptkonstruktor - f&uuml;r Client / Multiplayer<br>
	 * Erstellt Spieler mit Namen und der M&ouml;glichkeit, die Konsole zu benutzen sowie
	 * der Eigenschaft isBot = false
	 */

	public Gambler()
	{
		this.isBot = false;
		this.input = null;
	}

	public Gambler(String name)
	{
		this.name = name;
		this.isBot = false;
		this.input = null;
	}

	/**
	 * Entwicklungskonstruktor bzw. f&uuml;r Singleplayer
	 * 
	 * @param name
	 *            Spielername
	 * @param input
	 *            Eingabe des Namens des menschlichen Spielers
	 */
	public Gambler(String name, Scanner input)
	{
		this.name = name;
		this.isBot = false;
		this.input = input;
	}
	
	/** Versendet Karten und oder Nachrichten an den Client im Multiplayer
	 * @param topCard Oberste Karte
	 * @param msg1 Nachricht an Spieler
	 * @param msg2 Nachricht an Spieler
	 */

	public void send(Card topCard, String msg1, String msg2)
	{
		try
		{
			if(topCard == null && msg1 == null && msg2 == null)
			{
				ToClientHand msg = new ToClientHand(handCards);
	
				oos.writeObject(msg);
				oos.flush();
				oos.reset();
			}
			else if(topCard != null)
			{
				ToClientTopCard top = new ToClientTopCard(topCard);
				
				oos.writeObject(top);
				oos.flush();
				oos.reset();		
			}
			else if(msg1.equalsIgnoreCase("seperating"))
			{
				ToClientTurnSerparator nextTurn = new ToClientTurnSerparator(msg2);
				oos.writeObject(nextTurn);
				oos.flush();
				oos.reset();
			}
			else if(msg1.equalsIgnoreCase("drawError"))
			{
				ToClientCorrectCheck err = new ToClientCorrectCheck("drawError", false);
				oos.writeObject(err);
				oos.flush();
				oos.reset();
				
			}
			
			

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/** Sendet die gehobene Karte an den Client.
	 * @param drawnCard gezogene Karte
	 */
	
	public void sendDrawnCard(Card drawnCard)
	{
		try
		{
			ToClientDraw drawn = new ToClientDraw(drawnCard);
			
			oos.writeObject(drawn);
			oos.flush();
			oos.reset();	
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/** Zum versenden von Zwischennachrichten. Was alle anderen gemacht haben.
	 * @param bc Broadcast Nachricht an alle Spieler
	 */
	public void broadcast(Broadcast bc)
	{
		try
		{
			oos.writeObject(bc);
			oos.flush();
			oos.reset();	
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	/** Um den Clients das Ende des Spieles und den Gewinner mitzuteilen.
	 * @param end Ende des Spieles
	 */
	public void ending(ToClientEndOfGame end)
	{
		try
		{
			oos.writeObject(end);
			oos.flush();
			oos.reset();	
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	/**
	 * @param end Ende des Spieles
	 */
	public void sessionEnd(ToClientEndOfCurrentSession end)
	{
		try
		{
			oos.writeObject(end);
			oos.flush();
			oos.reset();	
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
	/**
	 * Abfrage ob weitergespielt werden soll
	 * @return weiterspielen true/false
	 */
	public boolean rematch()
	{
		try
		{
			ToServerRematch decision = (ToServerRematch) ois.readObject();
			
			return decision.getRematch();
		} 
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * Nimmt Karten in die Handkarten auf
	 * @param unoCard Karte vom Stapel
	 */
	public void cardPickup(Card unoCard)
	{
		handCards.add(unoCard);
		Collections.sort(handCards, gamblerHandSort);
	}

	/** Der Spieler zur Interaktion aufgefordert, so lange das Spiel l&auml;uft.<br>
	 * Die get&auml;tigte Interaktion wird verarbeitet und ein Feedback erfolgt.
	 * 
	 * @param topCard Oberste Karte vom Aktionsstapel (mit dieser muss interagiert werden)
	 * @return Spielerentscheidung
	 * @throws IOException -
	 * @throws ClassNotFoundException -
	 */
	public String getDecision(Card topCard) throws ClassNotFoundException, IOException
	{
		String decision = "";
		String wantedCardOrPlayer = "";

		boolean goingOn = true;

		while (goingOn == true)
		{
			ToServerDecision currentDecision = (ToServerDecision) ois.readObject();

			wantedCardOrPlayer = currentDecision.getWantedCard();
			decision = currentDecision.getDecision();

			// System.out.println("Bitte um Aktion. (play, draw, uno oder help sind
			// m�glich)");

			if (decision.equalsIgnoreCase("play"))
			{
				Card toLay = matchingCard(topCard, wantedCardOrPlayer);

				if (toLay == null)
				{
//					System.out.println("Gew&uuml;nschte Karte passt nicht oder Karte ist nicht in deiner Hand. Bitte um erneute Eingabe.");
					
					ToClientCorrectCheck check = new ToClientCorrectCheck("playFalse", false);
					oos.writeObject(check);
					oos.flush();
					oos.reset();
					
					goingOn = true;

				} else
				{
					goingOn = false;

//					System.out.println("Karte " + toLay.toString() + " wird gelegt.");  // Als BroadCast im gameturn
					
					decision = "play " + toLay.toString(); // play B 5
				}
			} else if (decision.equalsIgnoreCase("draw"))
			{
				goingOn = false;
				decision = "draw";
			} else if (decision.equalsIgnoreCase("uno"))
			{
				Card toLay = matchingCard(topCard, wantedCardOrPlayer);

				if (toLay == null)
				{
//					System.out.println("Gew&uuml;nschte Karte ist nicht in deiner Hand. Bitte um erneute Eingabe.");

					ToClientCorrectCheck check = new ToClientCorrectCheck("playFalse", false);
					oos.writeObject(check);
					oos.flush();
					oos.reset();
					
					send(null,  null,  null);
					
					goingOn = true;

				} else
				{
					goingOn = false;

//					System.out.println("Karte " + toLay.toString() + " wird gelegt und UNO wurde gerufen");
					
					decision = "uno  " + toLay.toString();
				}

			}else if(decision.equalsIgnoreCase("score")) 
			{
				
				ToClientScore score = new ToClientScore(db.getScoreFromCurrentRound(), db.getSessionID());
				oos.writeObject(score);
				oos.flush();
				oos.reset();
			}
			else if(decision.equalsIgnoreCase("history")) 
			{
				boolean existing = db.checkPlayerName(wantedCardOrPlayer);
				
				if(existing)
				{
					ToClientHistory hist = new ToClientHistory(db.getHistoryOfPlayer(wantedCardOrPlayer), db.getSessionID());
					oos.writeObject(hist);
					oos.flush();
					oos.reset();
				}
				else
				{
					ToClientCorrectCheck notExisting = new ToClientCorrectCheck("nameNotExisting", existing);
					oos.writeObject(notExisting);
					oos.flush();
					oos.reset();
				}
				
			}
			
			else
			{
				System.out.println("Ung�ltige Entscheidung, bitte um erneute Entscheidung.");
				goingOn = true;
			}
		}

		return decision;

	}

	/** Spieler wird bei einem Farbwechsel aufgefordert eine neue Farbe zu w&auml;hlen.
	 * 
	 * @return Gew&auml;hlte Farbe
	 */
	public char getNewColor()
	{
		Character color = 'X';
		
		
		
		while (color != 'R' || color != 'G' || color != 'B' || color != 'Y')
		{
//			System.out.println("Bitte um neue Farbe");
			try
			{
			ToClientColor dec = new ToClientColor();
			oos.writeObject(dec);
			oos.flush();	
				
				
			ToServerColor col = (ToServerColor) ois.readObject();
			color = col.getColor().charAt(0);
			
			color = Character.toUpperCase(color);

			if (color == 'R' || color == 'G' || color == 'B' || color == 'Y')
			{
				return color;
			} 
			else
			{
//				System.out.println("Farbe existiert nicht. Bitte erneut um Farbe.");
			}
			}
			catch(ClassNotFoundException | IOException e)
			{
				e.printStackTrace();
			}
		}
		return color;
	}

	/** Fragt ob die gehobene Karte abgelegt oder auf die Hand genommen werden soll.
	 * @param drawnCard Karte, die der Spieler ausspielen m&ouml;chte
	 * @param topCard Oberste Karte am Ablagestapel
	 * @return Information ob  Karte gelegt werden kann bzw Spiel weitergeht
	 */
	public boolean playDrawnCard(Card drawnCard, Card topCard)
	{
		sendDrawnCard(drawnCard);
		
//		System.out.println("Soll die Karte auf den Ablagestapel gelegt werden?  J / N  -  auch  Y / N  m�glich");
		
		try
		{
			ToServerDraw dec = (ToServerDraw) ois.readObject();
			boolean decision = dec.isToPlay();
			
			if (decision == true)
			{
				if (!topCard.match(drawnCard))
				{
					
//					System.out.println("Sch�ner Versuch, aber die Karte passt nicht um abgelegt zu werden. Sie wird jetzt den Handkarten hinzugef&uuml;gt.");
					send(null, "drawError", "Sch�ner Versuch, aber die Karte passt nicht um abgelegt zu werden. Sie wird jetzt den Handkarten hinzugef�gt.");
					
					cardPickup(drawnCard);
					return false;
				} 
				else
				{
					
					return true;
				}
			} 
			else if (decision == false)
			{
				cardPickup(drawnCard);
				return false;
			} 
			else
			{
				System.out.println("Fehler bei Ja / Nein Abfrage.");
			}
		} 
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}

	/** &Uuml;berpr&uuml;ft ob sich die gew&uuml;nschte oder eine zum ablegen passende Karte auf der Hand befindet.
	 * Das Format(Zeichen, Lerzeichen)
	 * bei der Eingabe der Karte muss genau &uuml;bereinstimmen R 5, Wld, WldDr,Y Rev
	 * 
	 * @param cardToCompare Karte die verglichen werden soll
	 * @param wantedCard gew&uuml;nschte Karte
	 * @return Eine Karte oder NULL wird an getDecesion zur&uuml;ckgegeben
	 */
	public Card matchingCard(Card cardToCompare, String wantedCard)
	{
		// System.out.println("Bitte um gew&uuml;nschte Karte. Format wie Karte, z.B.: R 7
		// oder G Skp oder WldDr,...");
		// String decision = input.nextLine();

		Card temp = convertStringToCard(wantedCard);

		if (temp != null)
		{
			for (Card card : handCards)
			{

				if (cardToCompare.match(temp) && temp.isSameCard(card))
				{
					Card result = card;
					handCards.remove(card);
					return result;
				} else
				{

				}
			}
		}

		return null;
	}

	/**
	 * Wandelt Eingabe in Karte um
	 * 
	 * @param input String der in Karte umgewandelt wird
	 * @return Karte
	 */
	private Card convertStringToCard(String input)
	{
		if (input.equalsIgnoreCase("WldDr"))
		{
			Card temp = new Card('P', 10);
			return temp;
		}

		else if (input.equalsIgnoreCase("Wld"))
		{
			Card temp = new Card('P', 11);
			return temp;
		}

		else if (input.length() == 5)
		{
			Card temp = null;
			Character color = input.charAt(0);
			color = Character.toUpperCase(color);

			String action = "" + input.charAt(2) + input.charAt(3) + input.charAt(4);

			if (action.equalsIgnoreCase("Skp"))
			{
				temp = new Card(color, 13);
			} else if (action.equalsIgnoreCase("Rev"))
			{
				temp = new Card(color, 14);
			}

			return temp;
		}

		else if (input.length() == 4)
		{
			Card temp = null;
			Character color = input.charAt(0);
			color = Character.toUpperCase(color);

			String action = "" + input.charAt(2) + input.charAt(3);

			if (action.equalsIgnoreCase("+2"))
			{
				temp = new Card(color, 12);
			}
			return temp;
		}

		else if (input.length() == 3)
		{
			Card temp = null;
			Character color = input.charAt(0);
			color = Character.toUpperCase(color);

			char digit = input.charAt(2);
			int number = Character.getNumericValue(digit);

			temp = new Card(color, number);
			return temp;
		}

		else
		{
			Card temp = null;
			return temp;
		}

	}

	/**
	 * Oberfl&auml;che der Karte (Rahmen und Platzhalter f&uuml;r Farbe und Nummer / Funktion)
	 * wird erstellt
	 * @param handArray Erstellen der Form der Handkarten
	 */
	public void printHandCards(ArrayList<Card> handArray)
	{
		for (int i = 0; i < handArray.size() * 5; i++)
		{
			if (i == 0 || i == 4)
			{

				System.out.println();

				for (int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.print("xxxxxxxxx ");
				}
			} else if (i == 1 || i == 3)
			{
				System.out.println();
				for (int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.print("x       x ");
				}
			} else if (i == 2)
			{
				System.out.println(); // System.out.print("\n");
				for (int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.printf("x %s x ", handArray.get(ctr).toString());
				}
			}
		}

	}
	
	/** Fordert den Spieler zu einer Handlung auf.
	 * @param topCard oberste Karte
	 * @return Spielerentscheidung
	 */
	
	public String getDecisionSinglePlayer(Card topCard)
	{
		boolean goingOn = true;

		String decision = "";

		while (goingOn == true)
		{
			System.out.println("Bitte um Aktion. (play, draw, uno, score, history oder help sind als Einagbe m�glich.)");

			decision = input.nextLine();
			decision = decision.trim();

			if (decision.equalsIgnoreCase("play"))
			{
				Card toLay = matchingCardSinglePlayer(topCard);

				if (toLay == null)
				{
					System.out.println(
							"Gew�nschte Karte passt nicht oder Karte ist nicht in deiner Hand. Bitte um erneute Eingabe.");
					goingOn = true;

				} else
				{
					goingOn = false;

					System.out.println("\r\nKarte " + toLay.toString() + " wird gelegt.");
					decision = "play " + toLay.toString(); // play B 5
				}
			} else if (decision.equalsIgnoreCase("draw"))
			{
				goingOn = false;
				decision = "draw";
			} else if (decision.equalsIgnoreCase("uno"))
			{
				Card toLay = matchingCardSinglePlayer(topCard);

				if (toLay == null)
				{
					System.out.println(
							"Gew�nschte Karte passt nicht oder Karte ist nicht in deiner Hand. Bitte um erneute Eingabe.");
					goingOn = true;

				} else
				{
					goingOn = false;

					System.out.println("\r\nKarte " + toLay.toString() + " wird gelegt und UNO wurde gerufen");
					decision = "uno " + toLay.toString();
				}

			} else if (decision.equalsIgnoreCase("help"))
			{
				getHelp();
				goingOn = true;

			}else if(decision.equalsIgnoreCase("score"))
			{
				db.getScoreFromCurrentRound();
				goingOn = true;
				
			}else if(decision.equalsIgnoreCase("history"))
			{
				System.out.println("Bitte um Namen des Spielers f�r dessen Score-History.\r\n  Name muss exakt so eingegeben werden wie er ausgegeben wird (Gro� / Kleinschreibung beachten. Name kann auch von der Console kopiert werden.)");
				
				String wantedPlayer = input.nextLine();
				wantedPlayer = wantedPlayer.trim();
				
				boolean existing = db.checkPlayerName(wantedPlayer);
				
				if(existing)
				{
					db.getHistoryOfPlayer(wantedPlayer);
				}
				else
				{
					System.out.println("Spieler mit dem gew�nschten Namen existiert nicht.");
				}
				
				goingOn = true;
			}
			
			else
			{
				System.out.println("Ung�ltige Entscheidung, bitte um erneute Entscheidung.");
				goingOn = true;
			}
		}

		return decision;

	}

	/**
	 * Sucht nach passender Karte durch vergleichen. Das Format(Zeichen, Lerzeichen)
	 * bei der Eingabe der Karte muss genau &uuml;bereinstimmen R 5, Wld, WldDr,Y Rev
	 * 
	 * @param cardToCompare Karte, die verglichen werden soll
	 * @return Eine Karte oder NULL wird an getDecesion zur&uuml;ckgegeben
	 */
	public Card matchingCardSinglePlayer(Card cardToCompare)
	{

		System.out.println("Bitte um gew�nschte Karte. Format wie Karte, z.B.: R 7 oder G Skp oder WldDr,...");
		String decision = input.nextLine();
		decision = decision.trim();

		Card temp = convertStringToCard(decision);

		if (temp != null)
		{
			for (Card card : handCards)
			{

				if (cardToCompare.match(temp) && temp.isSameCard(card))
				{
					Card result = card;
					handCards.remove(card);
					return result;
				} else
				{

				}
			}
		}

		return null;
	}

	public boolean playDrawnCardSinglePlayer(Card drawnCard, Card topCard)
	{
		System.out.println("Soll die Karte auf den Ablagestapel gelegt werden?  J / N  -  auch  Y / N  m�glich");

		boolean goingOn = true;

		while (goingOn == true)
		{
			String decision = input.nextLine();
			decision = decision.trim();

			if (decision.equalsIgnoreCase("J") || decision.equalsIgnoreCase("Y"))
			{
				if (!topCard.match(drawnCard))
				{
					goingOn = false;
					System.out.println(
							"Sch�ner Versuch, aber die Karte passt nicht um abgelegt zu werden. Sie wird jetzt den Handkarten hinzugef�gt.");
					cardPickup(drawnCard);
					return false;
				} else
				{
					goingOn = false;
					return true;
				}
			} else if (decision.equalsIgnoreCase("N"))
			{
				goingOn = false;
				cardPickup(drawnCard);
				return false;
			} else
			{
				goingOn = true;
				System.out.println("Fehler bei Ja / Nein Abfrage. Bitte um erneute Entscheidung.");
			}
		}
		return false;
	}
	
	/** Spieler wird bei einem Farbwechsel aufgefordert eine neue Farbe zu w&auml;hlen.
	 * 
	 * @return Gew&auml;hlte Farbe
	 */

	public char getNewColorSinglePlayer()
	{
		Character color = 'X';

		while (color != 'R' || color != 'G' || color != 'B' || color != 'Y')
		{
			System.out.println("Bitte um neue Farbe. R, G, B, Y als Eingabe m�glich.");
			String newColor = input.nextLine();
			newColor = newColor.trim();
			
			if(newColor.length() < 1 || newColor.length() > 2)
			{
				color = 'X';
			}
			else
			{
				color = newColor.charAt(0);
				color = Character.toUpperCase(color);
			}

			if (color == 'R' || color == 'G' || color == 'B' || color == 'Y')
			{
				return color;
			} 
			else
			{
				System.out.println("Farbe existiert nicht. Bitte erneut um Farbwahl.");
			}
		}
		return color;
	}

	public void clearHandCards()
	{
		handCards.clear();
	}

	/**
	 * Zeigt grundlegende Regeln und Bedienhinweise
	 */
	private void getHelp()
	{
		System.out.print("\nWillkommen bei UNO\r\n"
				+ "Das Ziel ist es, vor allen Anderen alle Handkarten auszuspielen.\r\n"
				+ "Gespielt werden kann nur eine auf die offene Karte passende Karte.\r\n"
				+ "Eine Karte ist passend, wenn die Farbe (Y, G, B, R und/oder die Zahl (1-9),\r\noder die Aktion (Wld, WldDr, Skp, Rev) �bereinstimmen.\r\n"
				+ "\r\nWeitere Bedingungen:\r\n\r\n" + "* Eine Eingabe wird mit der Return-Taste abgeschlossen.\r\n"
				+ "* Um eine Karte auszuspielen, Befehl 'play' eingegeben, Return-Taste,\r\n"
				+ "  dann die Kartenbezeichnung (z.B. 'Y 6') und dann wieder Return-Taste!\r\n"
				+ "* Die Eingabe der gew�hlten Karte muss exakt der Kartenbezeichnung entsprechen (auch Leerzeichen), z.B: 'R 1'.\r\n"
				+ "* Eine WldDr (Farbwechsel +4) kann nicht auf eine Wld (Farbwechsel) oder umgekehrt. \r\n"
				+ "* Wenn keine passende Karte auf der Hand ist, welche abgelegt werden kann, muss eine gezogen werden.\r\n"
				+ "* W�hrend man die vorletzte Karte ausspielt muss UNO gerufen werden. ACHTUNG! Anstelle des Befehls 'play'\r\n"
				+ "  wird der Befehl 'uno' gew�hlt und danach die Kartenbezeichnung eingegeben, z.B: 'uno'  \r\n"
				+ "\r\nBefehle \r\n\r\n" + "* play (spielen)\r\n" + "* draw (Karte ziehen)\r\n"
				+ "* help (diese Hilfe aufrufen)\r\n" + "* uno (uno rufen)\r\n" + "* score (aktueller Spielstand)\r\n" 
				+ "* history (meine Punktehistorie)\r\n\r\n" + "");

	}

	/**
	 * 
	 * @return Handkarten in einer Liste zur&uuml;ck
	 */
	public ArrayList<Card> getHand()
	{
		return this.handCards;
	}

	/**
	 * Holt Anzahl der Handkarten
	 * 
	 * @return Anzahl der Handkarten
	 */
	public int getHandSize()
	{
		return handCards.size();
	}

	/**
	 * Gibt Auskunft, ob es sich um einen Bot handelt
	 * 
	 * @return true, wenn Bot
	 */
	public boolean isBot()
	{
		return isBot;
	}

	public void setOis(ObjectInputStream ois)
	{
		this.ois = ois;
	}

	public void setOos(ObjectOutputStream oos)
	{
		this.oos = oos;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Gibt den Spielernamen zur&uuml;ck
	 * 
	 * @return Spielername
	 */
	public String getName()
	{
		return name;
	}

	public void setSocket(Socket s)
	{
		this.socket = s;
	}

	public String toString()
	{
		return name;
	}

}
