package at.campus02.unoserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.awt.Desktop;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import at.campus02.database.UNOSqliteDatabase;
import at.campus02.messages.ToClientEndOfCurrentSession;
import at.campus02.messages.ToServerRematch;
import at.campus02.unoserver.*;

/**
 * <em>Enth&auml;lt die eigentliche Anwendung "UnoApp"</em><br>
 * Verbindungsherstellung, Erstellen des GameManagers, Kartendesign, Session-Management
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */

public class UnoServer
{
	/**
	 * Erstellen des GameManagers, wartet auf Clients, fügt nach Ablauf der Zeit Bots hinzu<br>
	 * Eingeben des Spielernamens<br>
	 * Hinzuf&uuml;gen der Bots zum Spielertisch, wenn im Zeitrahmen keine menschlichen
	 * Spieler da sind<br>
	 * Austeilen der Karten<br>
	 * Initialisieren der Spielz&uuml;ge/Spielrunde (GameTurn)<br>
	 * @param args -
	 * @throws IOException -
	 */
	public static void main(String[] args) throws IOException
	{
		ServerSocket server = new ServerSocket(1111);
		UnoGameManagement gameManager = new UnoGameManagement();
		ArrayList<Gambler> botsList = botNameGenerator();
		
		UNOSqliteDatabase db = new UNOSqliteDatabase();
		db.createTable();
		int sessionID = db.getSessionID();
		int newSessionID = sessionID + 1;
		
		ArrayList<Thread> threads = new ArrayList<>();

		ObjectInputStream ois;
		ObjectOutputStream oos;

		server.setSoTimeout(10000); // 10sek

		Date start = null;

		while (gameManager.getGamblerSize() < 4)
		{
			try
			{
				Socket socket = server.accept();

				Gambler emptyGambler = new Gambler();
				gameManager.addGambler(emptyGambler);

				if (gameManager.getGamblerSize() == 0)
				{
					start = new Date();
				}
				ois = new ObjectInputStream(socket.getInputStream());
				oos = new ObjectOutputStream(socket.getOutputStream());

				ClientRunnable cr = new ClientRunnable(socket, gameManager, ois, oos, emptyGambler); //
				Thread t = new Thread(cr);
				threads.add(t);
				t.start();
			} catch (SocketTimeoutException e)
			{ 
				System.out.println("\r\nBot hinzugefuegt:");
				Date current = new Date();
				System.out.println("  " + botsList.get(0) + " joined." );
				gameManager.addGambler(botsList.remove(0));
				continue;
			}
		}

		System.out.println("\r\nAlle Spieler anwesend. (" + threads.size() + " Spieler und " + (4 - threads.size()) + " Bots)");

		for (Thread thr : threads)
		{
			try
			{
				thr.join();
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		
		System.out.println("Mögen die Spiele beginnen.");
		
		try
		{
			boolean goingOn = true;
						
			while(goingOn)
			{	
				if(db.getSessionID() != newSessionID) 
				{
					db.insertPlayers(newSessionID);
				}
				
				int roundCounter = 0;
				
				gameManager.clearDecks();
				gameManager.generateCards();
				gameManager.dealOutCards();
			
				while(gameManager.gameTurn())
				{
					roundCounter++;
				}
				
				db.updateNewScore(gameManager.getWinner(), newSessionID, gameManager.getPointsFromAllGamblers());
				
				
				
				
				int allPoints = gameManager.getPointsFromAllGamblers();
				
				System.out.println("Spiel endete nach " + roundCounter + " Zügen. Das entspricht ungefähr " + roundCounter/4 + " Runden.");
				System.out.println(gameManager.getWinner() + " bekommt " + allPoints + " Punkte\r\n");
			
				if (db.getMaxScoreFromCurrentRound() >= 500)
				{
					ToClientEndOfCurrentSession ending = new ToClientEndOfCurrentSession(true, db.getScoreFromCurrentRound(), db.getSessionID());
					
							newSessionID = db.getSessionID()+1;
					
					for (int i = 0; i < gameManager.gamblers.size(); i++)
					{
						if(!gameManager.gamblers.get(i).isBot()) 
						{
							gameManager.gamblers.get(i).sessionEnd(ending);
						}
					}
					
					for (int i = 0; i < gameManager.gamblers.size(); i++)
					{
						if(!gameManager.gamblers.get(i).isBot()) 
						{
							if(!gameManager.gamblers.get(i).rematch())
							{
								gameManager.gamblers.remove(i);
								gameManager.gamblers.add(botsList.remove(0));
							}
						}
					}
					
					int ctr = 0;
					
					for(int i = 0; i < gameManager.gamblers.size(); i++)
					{
						if(gameManager.gamblers.get(i).isBot())
						{
							ctr++;
						}
					}
					
					if(ctr > 3)
					{
						goingOn = false;
					}
				}
			}
			
			System.out.println("\r\nServer wird geschlossen. Bye Bye *wave*");
			server.close();
		} 
		catch (Exception e)
		{
			System.out.println("Error. Gameturn abgestürtzt " + e.getMessage());
			e.printStackTrace();
		}

	}

	
	
	/**  
	 * Liest aus der namen.txt Datei Namen ein und erstellt je nach Anfangsbuchstabe einen Bot. 
	 * @return Liste von Botnamen
	 */

	public static ArrayList<Gambler> botNameGenerator()
	{
		 FileReader fr;
		 BufferedReader br; 
		
		ArrayList<String> namen = new ArrayList<>();
		ArrayList<Gambler> botsList = new ArrayList<>();
		
		try {
			fr = new FileReader("namen.txt");
			br = new BufferedReader(fr);
			
			
			String next;
			
			while((next = br.readLine()) != null)
			{
				namen.add(next + " (Bot)");
			}
			
		}
		catch (IOException e) {
			
			e.printStackTrace();
		}

		Collections.shuffle(namen);

		for(int i = 0; i < 87; i++)
		{
			if (namen.get(i).charAt(0) == 'A')
			{
				botsList.add(new BotA(namen.get(i)));
			} else if (namen.get(i).charAt(0) == 'B')
			{
				botsList.add(new BotB(namen.get(i)));
			} else if (namen.get(i).charAt(0) == 'C')
			{
				botsList.add(new BotC(namen.get(i)));
			}
		}
		
		return botsList;
	}

}
