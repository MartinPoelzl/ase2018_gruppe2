package at.campus02.unoserver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.sqlite.core.DB;

import at.campus02.database.UNOSqliteDatabase;
import at.campus02.messages.*;
/** Verwaltet Spieler, KartenStapel und AblageStapel
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class UnoGameManagement
{

	protected static ArrayList<Gambler> gamblers = new ArrayList<>();
	private ArrayList<ClientRunnable> clients = new ArrayList<>();

	private ArrayList<Card> discardDeck = new ArrayList<>();
	private ArrayList<Card> deck = new ArrayList<>();
	
	private UNOSqliteDatabase db = new UNOSqliteDatabase();
	
	/**
	 * Einleitung der Erstellen des Decks
	 */
	public UnoGameManagement()
	{
		generateCards();
	}
	
	/**
	 * @return Anzahl der Spieler
	 */
	public int getGamblerSize()
	{
		return gamblers.size();
	}
	
	/** 
	 * Erstellt die 108 Karten f&uuml;r das Spiel 
	 */
	public void generateCards()
	{
		ArrayList<Integer> actionsCards = new ArrayList<>();
		actionsCards.add(14); // Rev
		actionsCards.add(13); // Skp
		actionsCards.add(12); // +2
		actionsCards.add(11); // Wld
		actionsCards.add(10); // +4
		// ArrayList von Aktionskarten, damit man mit einer Schleife alle generieren
		// kann.

		for (int i = 0; i < 10; i++) // Schleife für Farbige Zahlen 0-9, wobei 0er gibt es nur 1x pro Farbe
		{
			deck.add(new Card('R', i));
			deck.add(new Card('G', i));
			deck.add(new Card('B', i));
			deck.add(new Card('Y', i));

			if (i > 0)
			{
				deck.add(new Card('R', i));
				deck.add(new Card('G', i));
				deck.add(new Card('B', i));
				deck.add(new Card('Y', i));
			}
		}

		for (int ctr = 0; ctr < 2; ctr++)
		{
			for (int i = 0; i < 3; i++)
			{
				deck.add(new Card('R', actionsCards.get(i))); // Alle farbigen AktionsKarten
				deck.add(new Card('G', actionsCards.get(i)));
				deck.add(new Card('B', actionsCards.get(i)));
				deck.add(new Card('Y', actionsCards.get(i)));
			}
		}

		for (int ctr = 0; ctr < 4; ctr++)
		{
			for (int i = 3; i < 5; i++)
			{
				deck.add(new Card('P', actionsCards.get(i))); // +4 und Farbwechsel
			}
		}

		Collections.shuffle(deck); // Mischt das Deck
	}
	
	/** 
	 * Leert alle Stapel und Handkarten
	 */
	public void clearDecks()
	{
		deck.clear();
		discardDeck.clear();

		for (Gambler gmbl : gamblers)
		{
			gmbl.clearHandCards();
		}

	}
	
	/**	
	 * 	Leer die Liste der Spieler
	 */
	public void clearGamblers()
	{
		gamblers.clear();
	}

	/** 
	 * F&uuml;gt Spieler der Runde hinzu bist die Maximalzahl erreicht ist
	 * @param g Spieler
	 */
	public void addGambler(Gambler g)
	{
		if (4 >= gamblers.size())
		{
			gamblers.add(g);

		} else
		{
			System.out.println("Maximale Spieleranzahl erreicht.");
		}

		Collections.shuffle(gamblers); // Mischt die Spieler
	}

	/** Austeilen - gibt jedem Spieler 7 Karten<br>
	* Aufgedeckte Karte auf den AblageStapel, so es keine Aktionskarte ist.
	*/ 

	public void dealOutCards()
	{
		for (int i = 0; i < 7; i++)
		{
			for (int ctr = 0; ctr < gamblers.size(); ctr++)
			{
				Card card = drawCard();
				gamblers.get(ctr).cardPickup(card);
			}
		}

		Card temp = drawCard();

		while (temp.isActionCard() == true) // Sucht solange nach einer Karte die keine Aktionskarte ist
		{
			deck.add(temp);
			temp = drawCard();
		}

		discardDeck.add(temp); // Dann wird Karte als oberste offene Karte auf den Ablagestapel gelegt
	}

	/** Karte wird vom Stapel genommen. Wenn es keine zum ziehen gibt wird davor
	 *  der Ablagestapel, ausgenommen die oberste Karte davon, gemischt und als 
	 *  neuer Stapel verwendet.
	 * 
	 * @return oberste Karte vom Deck
	 */
	public Card drawCard()
	{
		if (deck.size() == 0)
		{
			Card showedCard = discardDeck.remove(0);

			for (Card lookingFor : discardDeck)
			{
				if (lookingFor.getNumber() == 10 || lookingFor.getNumber() == 11)
				{
					lookingFor.setCardColor('P');
				}
			}

			Collections.shuffle(discardDeck);

			deck.addAll(discardDeck);
			discardDeck.clear();
			discardDeck.add(showedCard);
		}

		return deck.remove(0);
	}

	/** Gibt die abzulegende Karte auf die offnene Karte des Ablagestapels.
	 * 
	 * @param cardToPut Karte die gelegt wird
	 */

	public void putCard(Card cardToPut)
	{
		discardDeck.add(0, cardToPut);
	}

	/** Spielzug für Multiplayer. Regelt einen Zug eines Bots oder Spielers
	 * 
	 * @return ob das Spiel noch l&auml;uft
	 * @throws ClassNotFoundException -
	 * @throws IOException -
	 */
	public boolean gameTurn() throws ClassNotFoundException, IOException
	{
		Integer size = gamblers.get(0).getHandSize();
		
		sendToAll(discardDeck.get(0), size.toString(), gamblers.get(0).getName());
		
		Gambler currentGambler = gamblers.remove(0);
		
		
		System.out.println("\n" + currentGambler + " ist jetzt am Zug. Auf der Hand werden "		
				+ currentGambler.getHandSize() + " Karten gehalten.");		//Anzeige nur auf dem Server !!!
		
		String currentDecision = "";

		Card cardToLay = null;
		String[] command;
		String initial = "";	
		
		System.out.println("Offene Karte:");		//Anzeige nur auf dem Server !!!
		printSingleCard(discardDeck.get(0));
		
		if (currentGambler.isBot() == false)
		{
			currentGambler.send(null, null, null);	// Sendet Hand an Spieler
			
			
			currentDecision = currentGambler.getDecision(discardDeck.get(0));
			command = currentDecision.split("\\s+");
			initial = command[0];
			String card = "";

			if (initial.contains("dra"))
			{
//				System.out.println("Hier deine Karte:");
				
				Card toDraw = drawCard();
				
//				printSingleCard(toDraw);
								
				if (currentGambler.playDrawnCard(toDraw, discardDeck.get(0)) == true)
				{
					putCard(toDraw);
					
					sendToAll(null, "playDrawn", toDraw.toString()); // Broadcast playdrawn
					
					if (toDraw.isActionCard() == true)
					{
						executeAction(toDraw, currentGambler);
					}
					gamblers.add(currentGambler);
					
					sendToAll(null, "seperating", null);	// -+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					
					return true;
				} else
				{
					sendToAll(null, "keepDrawn", currentGambler.getName());	// Broadcast keepdrawn			
					gamblers.add(currentGambler);
					sendToAll(null, "seperating", null);	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					return true;
				}

			}
			else
			{
				if (command.length == 3)
				{
					card = command[1] + " " + command[2];
				} else
				{
					card = command[1];
				}

				cardToLay = convertStringToCard(card);
				
			}
			
			
		}
		if (currentGambler.isBot() == true)
		{
			currentDecision = currentGambler.getDecision(discardDeck.get(0));
			command = currentDecision.split("\\s+");
			initial = command[0];
			String card = "";

			if (initial.contains("dra"))
			{
				Card toDraw = drawCard();
				if (currentGambler.playDrawnCard(toDraw, discardDeck.get(0)) == true)
				{
					putCard(toDraw);
					
					sendToAll(null, "playDrawn", toDraw.toString()); // Broadcast playdrawn
					
					if (toDraw.isActionCard() == true)
					{
						executeAction(toDraw, currentGambler);
					}

					gamblers.add(currentGambler);
					sendToAll(null, "seperating", null);	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					return true;

				} else
				{
					sendToAll(null, "keepDrawn", currentGambler.getName());	// Broadcast keepdrawn	
					gamblers.add(currentGambler);
					sendToAll(null, "seperating", null);	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
					return true;
				}
			} else
			{
				if (command.length == 3)
				{
					card = command[1] + " " + command[2];
				} else
				{
					card = command[1];
				}

				cardToLay = convertStringToCard(card);
			}
		}
		
		if(initial.equalsIgnoreCase("uno"))			//BRoadcast f&uuml;r uno rufen
		{
			sendToAll(null, "shoutUno", cardToLay.toString());	// Broadcast 
		}
		if(initial.equalsIgnoreCase("play"))
		{
			sendToAll(null, "playing", cardToLay.toString());	// Broadcast 
		}
		
		if ((!initial.equalsIgnoreCase("uno") && currentGambler.getHandSize() == 1)
				|| initial.equalsIgnoreCase("uno") && currentGambler.getHandSize() > 1)
		{
//			System.out.println("Uno wurde zum falschen Zeitpunkt gerufen. Hier hast du 4 Karten, spiel weiter. ^^");
			
			sendToAll(null, "+4", currentGambler.getName());		//Broadcast
			
			for (int ctr = 0; ctr < 4; ctr++)
			{
				currentGambler.cardPickup(drawCard());
			}
		}

		if (cardToLay == null)
		{
			System.out.println("Error cardToLay is NULL");
		} else
		{
			putCard(cardToLay);
			if (cardToLay.isActionCard() == true)
			{
				executeAction(cardToLay, currentGambler);
			}
		}

		if (currentGambler.getHandSize() == 0)
		{
			gamblers.add(0, currentGambler);
			
			sendToAll(null, "endOfGame", currentGambler.getName());		//Broadcast
			
			
			System.out.println("\r\n♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪\r\n");
			System.out.println("♪ღ♪\tDas Spiel ist zuende. Gewonnen hat: " + gamblers.get(0).getName());
			System.out.println("\r\n♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪\r\n");
			return false;
		}

		if (cardToLay.getNumber() != 13)
		{
			gamblers.add(currentGambler);
		} 
		else
		{
			gamblers.add(gamblers.size() - 1, currentGambler);
		}

		System.out.println("-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\t!!!!!   Server   !!!!!");
		sendToAll(null, "seperating", null);	//-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		return true;
	}

	/** 
	 * F&uuml;hrt die verschiedenen Aktionskarten aus
	 * @param foundCard gew&auml;hlte Karte
	 * @param currentGambler aktueller Spieler
	 */
	public void executeAction(Card foundCard, Gambler currentGambler)
	{
		int action = foundCard.getNumber();

		if (action == 14) // Rev
		{
			Collections.reverse(gamblers);		
			
			sendToAll(null, "Rev", gamblers.get(0).getName());		//Broadcast
		} 
		else if (action == 13) // Skp
		{
			Gambler temp = gamblers.remove(0);
			
			sendToAll(null, "skp", temp.getName());		//Broadcast
			
			gamblers.add(temp);
		} 
		else if (action == 12) // +2
		{
//			System.out.println(gamblers.get(0) + " bekommt 2 Karten");
			
			sendToAll(null, "+2", gamblers.get(0).getName());		//Broadcast
			
			for (int i = 0; i < 2; i++)
			{
				gamblers.get(0).cardPickup(drawCard());
			}
		} 
		else if (action == 11) // Wld
		{
			
			char newColor = currentGambler.getNewColor();
			if (newColor != 'X')
			{
				discardDeck.get(0).setCardColor(newColor);
			}
			
			sendToAll(null, "wld", "" + newColor);		//Broadcast
			
		} 
		else if (action == 10) // +4
		{

			char newColor = currentGambler.getNewColor();
			if (newColor != 'X')
			{
				discardDeck.get(0).setCardColor(newColor);
			}
			sendToAll(null, "wld", "" + newColor);		//Broadcast
			
//			System.out.println(gamblers.get(0) + " bekommt 4 Karten");
			
			sendToAll(null, "+4", gamblers.get(0).getName());		//Broadcast
			
			for (int i = 0; i < 4; i++)
			{
				gamblers.get(0).cardPickup(drawCard());
			}

		} 
		else
		{
			System.out.println("Action Card Error beim umsetzten der Action der Karte: " + foundCard.toString() + " "
					+ foundCard.getNumber());
		}

	}
	
	/** Informationen an alle Spieler (Broadcasts)
	 * 
	 * @param topCard oberste Karte
	 * @param message1 verschiedene Spieleraktionen
	 * @param message2 verschiedene Spieleraktionen
	 */
	private void sendToAll(Card topCard, String message1, String message2)
	{
		// Aufpassen: Bot-Abfrage  -  An Bots kann kein Broadcast gemacht werden...
		
		if(topCard != null)
		{
			Broadcast brcs = new Broadcast(topCard, message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("Rev"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);	// msg1 Rev, msg2 NextGambler
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("handSize"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);	// msg1 Rev, msg2 NextGambler
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("playDrawn"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("keepDrawn"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("playing"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("shoutUno"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("skp"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("wld"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("+4"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("+2"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
				}
			}
		}
		else if(message1.equalsIgnoreCase("shoutUno"))
		{
			Broadcast brcs = new Broadcast(message1, message2);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.broadcast(brcs);
					
				}
			}
		}
		else if(message1.equalsIgnoreCase("seperating"))
		{
//			String seperator = "\r\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+";
//			String seperator = "\r\n°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸¸,ø¤º°`°º¤ø,¸,ø¤°º¤ø,¸¸,ø¤º°`°º¤ø,¸";
			String seperator = "\r\n(>'-')> <('-'<) ^('-')^ <('-'<) (>'-')> <('-'<) ^('-')^ <('-'<) ^('-')^ <('-'<) (>'-')> <('-'<) ^('-')^ <('-'<)";
//			String seperator = "\r\n><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º> ><(((º>";
			
			
			
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.send(null, message1, seperator);
				}
			}
		}
		else if(message1.equalsIgnoreCase("endOfGame"))
		{
			Integer point = getPointsFromAllGamblers();
			String points = point.toString();
			
			ToClientEndOfGame end = new ToClientEndOfGame(false, message2, points);
			for (Gambler gamb : gamblers)
			{
				if(!gamb.isBot())
				{
					gamb.ending(end);
					
				}
			}
		}
		
		
	}
	
	/** Wandelt einen String in eine Karte um.
	 * 
	 * @param input Infos f&uuml;r Karte
	 * @return Karte
	 */
	private Card convertStringToCard(String input)
	{
		if (input.equalsIgnoreCase("WldDr"))
		{
			Card temp = new Card('P', 10);
			return temp;
		}

		else if (input.equalsIgnoreCase("Wld"))
		{
			Card temp = new Card('P', 11);
			return temp;
		}

		else if (input.length() == 5)
		{
			Card temp = null;
			Character color = input.charAt(0); // b
			Character.toUpperCase(color);
			String action = "" + input.charAt(2) + input.charAt(3) + input.charAt(4);

			if (action.equalsIgnoreCase("Skp"))
			{
				temp = new Card(color, 13);
			} else if (action.equalsIgnoreCase("Rev"))
			{
				temp = new Card(color, 14);
			}

			return temp;
		} else if (input.length() == 4)
		{
			Card temp = null;
			Character color = input.charAt(0); // b
			Character.toUpperCase(color);

			temp = new Card(color, 12);

			return temp;
		}

		else if (input.length() == 3)
		{

			Character color = input.charAt(0);
			Character.toUpperCase(color);

			char digit = input.charAt(2);
			int number = Character.getNumericValue(digit); // Konvertiert Char -> Int

			Card temp = new Card(color, number);
			return temp;
		}

		else
		{
			System.out.println("Error - convertStrigToCard");
			return null;
		}

	}
	
	/** GameTurn f&uuml;r Einzelspieler. 
	 * Regelt Ablauf eines Zuges von Spieler und Bots
	 * 
	 * @return gameTurnSinglePayer true/false
	 */
	public boolean gameTurnSinglePayer()
	{
		Gambler currentGambler = gamblers.remove(0);
		System.out.println("\n" + currentGambler + " ist jetzt am Zug. Auf der Hand werden "
				+ currentGambler.getHandSize() + " Karten gehalten.");
		String currentDecision = "";

		Card cardToLay = null;
		String[] command;
		String initial = "";
		if(discardDeck.get(0).getNumber() == 10 || discardDeck.get(0).getNumber() == 11)
		{
			System.out.println("\rDie Farbe der offenen Karte ist: " + discardDeck.get(0).getColor() + "\r");
		}
		else
		{
			System.out.println("Offene Karte:");
		}
		printSingleCard(discardDeck.get(0));

		if (currentGambler.isBot() == true)
		{
			currentDecision = currentGambler.getDecisionSinglePlayer(discardDeck.get(0));
			command = currentDecision.split("\\s+");
			initial = command[0];
			String card = "";

			if (initial.contains("dra"))
			{
				Card toDraw = drawCard();
				if (currentGambler.playDrawnCardSinglePlayer(toDraw, discardDeck.get(0)) == true)
				{
					putCard(toDraw);
					if (toDraw.isActionCard() == true)
					{
						executeActionSinglePlayer(toDraw, currentGambler);
					}

					if (toDraw.getNumber() != 13)
					{
						gamblers.add(currentGambler);
					} else
					{
						gamblers.add(gamblers.size() - 1, currentGambler);
					}
					
					System.out.println("\r\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
					return true;

				} else
				{
					// Karte wurde in Hand genommen
					gamblers.add(currentGambler);
					System.out.println("\r\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
					return true;
				}
			} else
			{
				if (command.length == 3)
				{
					card = command[1] + " " + command[2];
				} else
				{
					card = command[1];
				}

				cardToLay = convertStringToCard(card);
			}
		}

		if (currentGambler.isBot() == false)
		{
			printHandCards(currentGambler.getHand());
			currentDecision = currentGambler.getDecisionSinglePlayer(discardDeck.get(0));
			command = currentDecision.split("\\s+");
			initial = command[0];
			String card = "";

			if (initial.contains("dra"))
			{
				System.out.println("Hier ist deine gehobene Karte:");
				// sendMessageWithResponse(); retun:desicion
				// zweite Methode --> sendMessage
				Card toDraw = drawCard();
				printSingleCard(toDraw);
				if (currentGambler.playDrawnCardSinglePlayer(toDraw, discardDeck.get(0)) == true)
				{
					putCard(toDraw);
					if (toDraw.isActionCard() == true)
					{
						executeActionSinglePlayer(toDraw, currentGambler);
					}
					if (toDraw.getNumber() != 13)
					{
						gamblers.add(currentGambler);
					} else
					{
						gamblers.add(gamblers.size() - 1, currentGambler);
					}
					System.out.println("\r\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
					return true;
				} 
				else
				{
					gamblers.add(currentGambler);
					System.out.println("\r\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
					return true;
				}

			} else
			{
				if (command.length == 3)
				{
					card = command[1] + " " + command[2];
				} else
				{
					card = command[1];
				}

				cardToLay = convertStringToCard(card);
			}
		}

		if ((!initial.equalsIgnoreCase("uno") && currentGambler.getHandSize() == 1)
				|| initial.equalsIgnoreCase("uno") && currentGambler.getHandSize() > 1)
		{
			System.out.println("Uno wurde zum falschen Zeitpunkt gerufen. Hier hast du 4 Karten, spiel weiter. :-P");
			for (int ctr = 0; ctr < 4; ctr++)
			{
				currentGambler.cardPickup(drawCard());
			}
		}

		if (cardToLay == null)
		{
			System.out.println("Error cardToLay is NULL");
		} else
		{
			putCard(cardToLay);
			if (cardToLay.isActionCard() == true)
			{
				executeActionSinglePlayer(cardToLay, currentGambler);
			}
		}

		if (currentGambler.getHandSize() == 0)
		{
			gamblers.add(0, currentGambler);
			
			System.out.println("\r\n♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪\r\n");
			System.out.println("♪ღ♪\tDas Spiel ist zuende. Gewonnen hat: " + gamblers.get(0).getName());
			System.out.println("\r\n♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪\r\n");
			return false;
		}

		if (cardToLay.getNumber() != 13)
		{
			gamblers.add(currentGambler);
		} else
		{
			gamblers.add(gamblers.size() - 1, currentGambler);
		}

		System.out.println("\r\n-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
		return true;
	}
	
	/** 
	 * Diese Methode f&uuml;hrt die verschiedenen Aktionskarten f&uuml;r den Singleplayer aus.
	 * @param foundCard gew&auml;hlte Karte
	 * @param currentGambler aktueller Spieler
	 */

	public void executeActionSinglePlayer(Card foundCard, Gambler currentGambler)
	{
		int action = foundCard.getNumber();

		if (action == 14) // Rev
		{
			Collections.reverse(gamblers);
			System.out.println("Die Richtung hat sich geändert. Als nächstes an der Reihe ist: " + gamblers.get(0));
			
		} else if (action == 13) // Skp
		{
			System.out.println(gamblers.get(0) + " wird ausgelassen.");
			Gambler temp = gamblers.remove(0);
			gamblers.add(temp);
		} else if (action == 12) // +2
		{
			System.out.println(gamblers.get(0) + " bekommt 2 Karten.");
			for (int i = 0; i < 2; i++)
			{
				gamblers.get(0).cardPickup(drawCard());
			}
		} else if (action == 11) // Wld
		{
			char newColor;
			
			if(currentGambler.isBot())
			{
				newColor = currentGambler.getNewColor();
			}
			else
			{
				newColor = currentGambler.getNewColorSinglePlayer();				
			}
			
			if (newColor != 'X')
			{
				foundCard.setCardColor(newColor);
			}
		} else if (action == 10) // +4
		{
			char newColor;
			
			if(currentGambler.isBot())
			{
				newColor = currentGambler.getNewColor();
			}
			else
			{
				newColor = currentGambler.getNewColorSinglePlayer();				
			}
			
			if (newColor != 'X')
			{
				foundCard.setCardColor(newColor);
			}
			System.out.println(gamblers.get(0) + " bekommt 4 Karten.");
			for (int i = 0; i < 4; i++)
			{
				gamblers.get(0).cardPickup(drawCard());
			}

		} else
		{
			System.out.println("Action Card Error beim umsetzten der Action der Karte: " + foundCard.toString() + " "
					+ foundCard.getNumber());
		}

	}
	
/**
 * @return History
 */
	public HashMap<String, String> completeHistory()
	{
		HashMap<String, String> result = new HashMap<>();
		
		return result;
	}
	
	/** 
	 * Berechnet die Punkte, welche der Gewinner bekommt.
	 * @return Punkte aller Spieler
	 */
	public int getPointsFromAllGamblers()
	{
		int points = 0;

		for (Gambler gmbl : gamblers)
		{
			ArrayList<Card> tempHand = gmbl.getHand();
			if (tempHand != null)
			{
				for (Card card : tempHand)
				{
					points += card.getCardPoints();
				}
			}
		}

		return points;
	}
	
	/** 
	 * Liefert am Ende des Spiels den Namen des Gewinners 
	 * @return Namen des Gewinners 
	 */
	
	public Gambler getWinner()
	{
		return gamblers.get(0);
	}

	public static ArrayList<Gambler> getGamblers()
	{
		return gamblers;
	}
	
	/** 
	 *Verwandelt eine Karte in eine Consolen-Ausgabe.
	 * @param toPrint Karte
	 */
	private void printSingleCard(Card toPrint)
	{

		System.out.println("\r\nxxxxxxxxx ");
		System.out.println("x       x ");
		System.out.printf("x %s x \n", toPrint.toString());
		System.out.println("x       x ");
		System.out.println("xxxxxxxxx \r\n");

	}
	
	/** 
	*Verwandelt alle Handkarten in eine Consolen-Ausgabe.
	 * @param handArray Handkarten[]
	 */

	public void printHandCards(ArrayList<Card> handArray)
	{
		for (int i = 0; i < handArray.size() * 5; i++)
		{
			if (i == 0 || i == 4)
			{

				for (int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.print("xxxxxxxxx ");
				}
				System.out.println();
			} 
			else if (i == 1 || i == 3)
			{
				for (int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.print("x       x ");
				}
				System.out.println();
			} 
			else if (i == 2)
			{
				for (int ctr = 0; ctr < handArray.size(); ctr++)
				{
					System.out.printf("x %s x ", handArray.get(ctr).toString());
				}
				System.out.println();
			}
		}
		System.out.println();
	}

	public Map<String, Integer> getScores()
	{
		return null;
	}
}
