package at.campus02.unoserver;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;

/**
 * <em>BotB hat eine komplexere Spiellogik</em><br>
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;
 * 
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 */
public class BotB extends Gambler implements BotInterface
{
	protected String name;
	private boolean isBot;
	protected ArrayList<Card> handCards = new ArrayList<>();
	
	private Comparator<Card> BotBStrg = new Comparator<Card>()
	{
		@Override
		public int compare(Card card1, Card card2)
		{
			if (card1.getNumber() == 10 || card1.getNumber() == 11)
			{
				return 1;
			}

			if (card1.getNumber() > card2.getNumber())
			{
				return 1;
			}
			if (card1.getNumber() < card2.getNumber())
			{
				return -1;
			}

			if (card1.getColor() < card2.getColor())
			{
				return 1;
			}

			if (card1.getColor() > card2.getColor())
			{
				return -1;
			}

			return 0;
		}
	};

	/**
	 * Bots bestehen aus name und dem Marker isBot=true;
	 * @param name Bot-Name
	 */
	public BotB(String name)
	{
		super(name, null);

		this.name = name;
		this.isBot = true;

	}

	/**
	 * Bots k�nnen Karten aufnehmen
	 */
	public void cardPickup(Card unoCard)
	{
		handCards.add(unoCard);
		howToSort();
	}

	/**
	 *  Sortieren der Handkarten des Bots anhand des eingebauten Comperators!
	 */
	public void howToSort()
	{
		Collections.sort(handCards, BotBStrg);

	}

	/**
	 * Bei weniger als vier Handkarten: <br>
	 * 
	 */
	public Card matchingCard(Card cardToCompare)
	{
		Gambler temp = UnoGameManagement.getGamblers().get(0);

		if (temp.getHandSize() < 4)
		{
			for (Card card : handCards)
			{
				if (cardToCompare.getNumber() != 11 && card.getNumber() == 10)
				{
					handCards.remove(card);
					if (handCards.size() == 1)
					{
						System.out.println("UNO!");
					}
					return card;
				} 
				else if ((cardToCompare.getColor() == card.getColor() && card.getNumber() == 12) || (cardToCompare.getNumber() == card.getNumber() && card.getNumber() == 12))
				{
					handCards.remove(card);
					if (handCards.size() == 1)
					{
						System.out.println("UNO!");
					}
					return card;
				}
			}
		}

		for (Card card : handCards)
		{
			if (cardToCompare.getNumber() == card.getNumber() && cardToCompare.getColor() != card.getColor())
			{
				handCards.remove(card);
				if (handCards.size() == 1)
				{
					System.out.println("UNO!");
				}
				return card;
			}
		}

		for (Card card : handCards)
		{
			if (cardToCompare.getNumber() == card.getNumber())
			{
				handCards.remove(card);
				if (handCards.size() == 1)
				{
					System.out.println("UNO!");
				}
				return card;
			}
		}

		for (Card card : handCards)
		{
			if (cardToCompare.match(card))
			{
				handCards.remove(card);
				if (handCards.size() == 1)
				{
					System.out.println("UNO!");
				}
				return card;
			}
		}
		return null;
	}

	/**
	 * Bot-Interaktion<br>
	 * <ul>
	 * <li>zieht eine Karte - Befehl draw</li>
	 * <li>ruft UNO und legt vorletzte Karte - Befehl uno</li>
	 * <li>spielt Karte - Befehl play</li>
	 * </ul>
	 */
	@Override
	public String getDecision(Card topCard)
	{
		Card toLay = matchingCard(topCard);
		if (toLay == null)
		{
//			System.out.println(this.toString() + " zieht eine Karte.");
			return "draw ";
		} else if (handCards.size() == 1)
		{
//			System.out.println(this.toString() + ": ruft UNO und legt: " + toLay.toString());
			return "uno " + toLay.toString();
		} else
		{
//			System.out.println(this.toString() + ": spielt " + toLay.toString());
			return "play " + toLay.toString();
		}

	}

	/**
	 * Entscheidung NACH dem Heben:<br>
	 * <ul>
	 * <li>hebt und spielt die Karte</li>
	 * <li>beh&auml;lt die gehobene Karte in der Hand</li>
	 * </ul>
	 */
	public boolean playDrawnCard(Card drawnCard, Card topCard)
	{

		if (topCard.match(drawnCard))
		{
//			System.out.println(this.toString() + " hebt und spielt die Karte: " + drawnCard);
			return true;
		} else
		{
//			System.out.println(this.toString() + " beh&auml;lt die gehobene Karte in der Hand.");
			cardPickup(drawnCard);
			return false;
		}

	}
	
	/**
	 * Entscheidungsfindung des Bots im Singleplayer
	 */
	public String getDecisionSinglePlayer(Card topCard)
	{
		Card toLay = matchingCard(topCard);
		if (toLay == null)
		{
			System.out.println(this.toString() + " zieht eine Karte.");
			return "draw ";
		} else if (handCards.size() == 1)
		{
			System.out.println(this.toString() + ": ruft UNO und legt: " + toLay.toString());
			return "uno " + toLay.toString();
		} else
		{
			System.out.println(this.toString() + ": spielt " + toLay.toString());
			return "play " + toLay.toString();
		}

	}
	/**
	 *  Ob gehobene Karte ausgesoielt wird oder auf der Hand bleibt
	 */
	public boolean playDrawnCardSinglePlayer(Card drawnCard, Card topCard)
	{

		if (topCard.match(drawnCard))
		{
			System.out.println(this.toString() + " hebt und spielt die Karte: " + drawnCard);
			return true;
		} else
		{
			System.out.println(this.toString() + " behw�hltlt die gehobene Karte in der Hand.");
			cardPickup(drawnCard);
			return false;
		}

	}

	/**
	 * Farbwechsel
	 * 
	 * @return gibt zur&uuml;ck, auf welche Farbe gewechselt wurde
	 */
	public char getNewColor()
	{
		int ctrMax = 0;
		int[] RGBY = new int[4];

		RGBY[0] = 0;
		RGBY[1] = 0;
		RGBY[2] = 0;
		RGBY[3] = 0;

		for (Card card : handCards)
		{
			if (card.getColor() == 'R')
			{
				RGBY[0] += 1;
			} else if (card.getColor() == 'G')
			{
				RGBY[1] += 1;
			} else if (card.getColor() == 'B')
			{
				RGBY[2] += 1;
			} else if (card.getColor() == 'Y')
			{
				RGBY[3] += 1;
			}

		}
		for (int i = 0; i < 4; i++)
		{
			if (ctrMax < RGBY[i])
			{
				ctrMax = RGBY[i];
			}
		}
		for (int i = 0; i < 4; i++)
		{
			if (ctrMax == RGBY[i])
			{
				if (i == 0)
				{
					System.out.println(this.toString() + " w�hlt die Farbe: R");
					return 'R';
				} else if (i == 1)
				{
					System.out.println(this.toString() + " w�hlt die Farbe: G");
					return 'G';
				} else if (i == 2)
				{
					System.out.println(this.toString() + " w�hlt die Farbe: B");
					return 'B';
				} else if (i == 3)
				{
					System.out.println(this.toString() + " w�hlt die Farbe: Y");
					return 'Y';
				}
			}
		}

		return 'Y';

	}
	
	/**
	 * 
	 * @return Handkarten in einer Liste zur&uuml;ck
	 */
	public ArrayList<Card> getHand()
	{
		return this.handCards;
	}
	
	/**
	 * @return Gibt die Anzahl der Handkarten zur&uuml;ck
	 */
	public int getHandSize()
	{
		return handCards.size();
	}
	
	public void clearHandCards()
	{
		handCards.clear();
	}

	/**
	 * @return gibt zur&uuml;ck, ob es sich um Bot handelt, oder nicht
	 */
	public boolean isBot()
	{
		return isBot;
	}

}
