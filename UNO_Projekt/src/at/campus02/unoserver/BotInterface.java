package at.campus02.unoserver;

/**
 * Erm�glicht es, dass Bots Karten in die "Hand" nehmen k�nnen und das Suchen und Umwandeln in eine Stelle im Array der Handkarten. Bot kann Ablegeentscheidung  treffen und darauf folgend eine Karte aus der Hand w&auml;hlen.
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 */
public interface BotInterface
{
	/**
	 * @param unoCard  Bots k�nnen Karten aufnehmen
	 */
	void cardPickup(Card unoCard);

	/**
	 * Bots k�nnen Entscheidungen treffen
	 * @param topCard Oberste Karte
	 * @return String Karte zum Ablegen zur&uuml;ck.
	 */
	String getDecision(Card topCard);

	/**
	 * Suche nach passender Karte zum auspielen
	 * @param cardToCompare Karte mit der vergleichen werden soll
	 * @return gibt dann die Karte an das Spiel zur&uuml;ck
	 */
	public Card matchingCard(Card cardToCompare);
}
