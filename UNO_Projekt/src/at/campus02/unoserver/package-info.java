/** Enth&auml;lt <em>Spielelemente (Spieler, Bots, Karten) und den GameManager</em><br>
* Im GameManager sind die Spielz&uuml;ge (GameTurns) enthalten.
* 
*  @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
*/
package at.campus02.unoserver;