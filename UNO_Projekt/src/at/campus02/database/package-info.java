/**Enth&auml;lt die <em>Datenbankanbindung, den Rundenz&auml;hler und die DB
 * selbst</em><br>
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Martin P&ouml;lzl, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 */
package at.campus02.database;