package at.campus02.database;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import at.campus02.unoserver.Gambler;
import at.campus02.unoserver.UnoGameManagement;

/**Erstellen der Datenbank
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Martin P&ouml;lzl, Veronika
 *         Strau&szlig;
 * 
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 */
public class UNOSqliteDatabase implements Serializable
{
	
	private static final long serialVersionUID = 9172214151380089184L;
	private SqliteClient dbUnoScore;

	/**
	 * <em>Erstellt einen neuen SQLClient</em> um auf die Datenbank zuzugreifen.
	 */
	public UNOSqliteDatabase()
	{
		try
		{
			dbUnoScore = new SqliteClient("unoHighscore.sqlite");

		} catch (SQLException e)
		{
			System.out.println("Datenbank konnte nicht erstellt werden");
		}
	}

	/**
	 * <em>Datenbank wird erstellt,</em> so noch keine vorhanden ist.
	 */
	public void createTable()
	{
		try
		{

			String createTable = "CREATE TABLE Highscore (Player varchar(100) NOT NULL, Session int NOT NULL DEFAULT 0, Score int NOT NULL DEFAULT 0, CONSTRAINT PK_Sessions PRIMARY KEY (Player, Session));";
			if (!dbUnoScore.tableExists("Highscore"))
			{
				dbUnoScore.executeStatement(createTable);
				String insertDefault = "INSERT INTO Highscore (Player, Session) VALUES ('start', 0);";
				dbUnoScore.executeStatement(insertDefault);
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 *  <em>Playerdaten werden in Datenbank eingef&uuml;gt</em><br>
	 * &Uuml;ber das GameManagement wird der Spielername bezogen und mit der Session- und
	 * Runden-Nummer abgespeichert.<br>
	 * @param sessionID sessionID des Spielers
	 */
	public void insertPlayers(int sessionID)
	{

		String g1 = UnoGameManagement.getGamblers().get(0).getName();
		String g2 = UnoGameManagement.getGamblers().get(1).getName();
		String g3 = UnoGameManagement.getGamblers().get(2).getName();
		String g4 = UnoGameManagement.getGamblers().get(3).getName();

		String insertPlayer1 = "INSERT INTO Highscore (Player, Session) VALUES ('%s', %d), ('%s', %d), ('%s', %d), ('%s', %d);";

		try
		{
			dbUnoScore.executeStatement(
					String.format(insertPlayer1, g1, sessionID, g2, sessionID, g3, sessionID, g4, sessionID));

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Bezieht die Sessions der Spieler
	 * @return Integer SessionID
	 */
	public int getSessionID()
	{

		String selectSessionID = "SELECT Session FROM Highscore ORDER BY Session DESC LIMIT 1";

		try
		{
			String result = dbUnoScore.executeStringQuery(selectSessionID);
			int sessionID = Integer.parseInt(result);

			return sessionID;

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	/**
	 * Holt den aktuellen Spielstand
	 * @param g Spieler
	 * @param sessionID SessionID des Spielers
	 * @return Punkte
	 */
	public int getCurrentScore(Gambler g, int sessionID)
	{

		String selectScoreGambler = "SELECT Score FROM Highscore WHERE PLayer ='%1s' AND Session= %2d ";

		try
		{
			String result = dbUnoScore.executeStringQuery(String.format(selectScoreGambler, g.getName(), sessionID));
			int score = Integer.parseInt(result);

			return score;

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

		return 0;
	}

	/**
	 * Am Ende einer Runde wird der neue Score mittels SQL-Update-Statement in der
	 * Datenbank hinzugef&uuml;gt
	 * @param winner Gewinner der Runde
	 * @param sessionID ID des Spielers
	 * @param currentPoints aktuelle Punkte
	 */
	public void updateNewScore(Gambler winner, int sessionID, int currentPoints)
	{

		try
		{
			String insertNewScore = "UPDATE Highscore SET Score= %1d WHERE Player= '%2s' AND Session= %3d;";
			int oldscore = getCurrentScore(winner, sessionID);
			int pointSum = currentPoints + oldscore;
			String result = String.format(insertNewScore, pointSum, winner.getName(), sessionID);

			dbUnoScore.executeStatement(result);

		} catch (SQLException e)
		{
			e.printStackTrace();
		}

	}


	/**
	 * @param gambler Spieler
	 * @return Gesamte Historie eines Spielers sortiert nach Datum.
	 */
	public ArrayList<HashMap<String, String>> getHistoryOfPlayer(String gambler)
	{
		String selectByPlayer = "SELECT Player, Session, Score FROM Highscore WHERE Player = '%1s';";

		ArrayList<HashMap<String, String>> results;
		try
		{
			results = dbUnoScore.executeQuery(String.format(selectByPlayer, gambler));
			for (HashMap<String, String> map : results)
			{
				System.out.println("In Session " + map.get("Session") + " hat Player " + map.get("Player") + " "
						+ map.get("Score") + " Punkte");
			}
			
			return results;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return null;
		
	}

	/**
	 * @return aktueller Punktestand aller Spieler.
	 */
	public ArrayList<HashMap<String, String>> getScoreFromCurrentRound()
	{

		String selectCurrentRound = "SELECT Player, Score FROM Highscore WHERE Session= %1d;";

		ArrayList<HashMap<String, String>> results;
		try
		{
			String stringresult = String.format(selectCurrentRound, getSessionID());
			results = dbUnoScore.executeQuery(stringresult);
			for (HashMap<String, String> map : results)
			{
				System.out.println(map.get("Player") + " hat derzeit in der Session "+ getSessionID()+ ": " + map.get("Score") + " Punkte");

			}
			return results;

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Pr&uuml;fen ob Spielername in DB existiert
	 * @param name Spieler-Name
	 * @return boolean existiert/existiert nicht
	 */	
	public boolean checkPlayerName(String name) 
	{
		String selectPlayer = "SELECT Player FROM Highscore WHERE Player= '%1s';";
		
		try
		{
			String result = dbUnoScore.executeStringQuery(String.format(selectPlayer, name));
			if(result != null) {
				return true;
			}	
			return false;
		} 
		catch (SQLException e)
		{
			e.printStackTrace();
		}	
		return false;
	}

	/**
	 * @return maxScore H�chsten Punktestand der aktuellen Runde
	 */
	public int getMaxScoreFromCurrentRound()
	{
		String selectMaxScore = "SELECT Score FROM Highscore WHERE Session= %1d ORDER BY Score DESC LIMIT 1;";
		try
		{
			String result = dbUnoScore.executeStringQuery(String.format(selectMaxScore, getSessionID()));
			int maxScore = Integer.parseInt(result);
			return maxScore;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return 0;
		}
	}
	
	/**
	 * 
	 * @return Name des Sessiongewinners
	 */
	public String getNameOfSessionWinner() {
		String selectSessionWinner = "SELECT Player FROM Highscore WHERE Session= %1d AND Score= %2d;";
		try
		{
			String nameSessionWinner = dbUnoScore.executeStringQuery(String.format(selectSessionWinner, getSessionID(), getMaxScoreFromCurrentRound()));
			return nameSessionWinner;
		} catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		
	}

}
