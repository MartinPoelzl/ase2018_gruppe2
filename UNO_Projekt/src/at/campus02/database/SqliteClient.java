package at.campus02.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
/**Herstellen der Verbindung zur Datenbank(DB), Pr&uuml;ft Existenz der DB und liest aus.
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;
 * 
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 */
public class SqliteClient {
	private Connection connection = null;
	
	
	/**
	 * Baut die Verbindung zur Datenbank auf
	 * @param dbName Bezeichnung der Art von Datenbank (jdbc:sqlite)
	 * @throws SQLException wird geworfen, wenn die Verbindung nicht erstellt werden kann
	 */
	public SqliteClient(String dbName) throws SQLException{
		connection = DriverManager.getConnection("jdbc:sqlite:" + dbName);		
	}
	
	/**
	 * Liest die Datenbank aus und &uuml;berpr&uuml;ft, ob Tabelle bereits vorhanden ist
	 * @param tableName Bezeichnung der Datenbank
	 * @return boolean: ob Tabelle bereits existiert oder nicht 
	 * @throws SQLException wenn kein g&uuml;ltiges SQL Statement gefunden wird
	 */
	public boolean tableExists(String tableName) throws SQLException{
		String query = "SELECT name FROM sqlite_master WHERE type='table' AND name='"+ tableName+"';";
		return executeQuery(query).size() > 0;
	}
	/**
	 * F&uuml;hrt das SQL Statement aus.<br>
	 * Timeout nach 30 Sekunden. <br>
	 * F&uuml;hrt Update des Stamentens aus<br> 
	 * @param sqlStatement auszuf&uuml;hrendes sqlStatement
	 * @throws SQLException f&uuml;r den Fall eines DB-Fehlers
	 */
	public void executeStatement(String sqlStatement) throws SQLException{
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		statement.executeUpdate(sqlStatement);
	}
	
	/**
	 * F&uuml;hrt sqlQuery aus und gibt einen String als Resultat.
	 * @param sqlQuery Warteschlange
	 * @return String sqlStatement auszuf&uuml;hrendes sqlStatement
	 * @throws SQLException f&uuml;r den Fall eines DB-Fehlers
	 */
	public String executeStringQuery(String sqlQuery) throws SQLException{
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		ResultSet rs = statement.executeQuery(sqlQuery);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		String result = rs.getString(columns);
		
		return result;
	}
	
/**
 * F&uuml;hrt sqlQuery aus und gibt die gefundenden Daten als Hashmap wieder
 * @throws SQLException -
 * @return gefundenden Daten als Hashmap wieder
 * @param sqlQuery -
 */

	public ArrayList<HashMap<String, String>> executeQuery(String sqlQuery) throws SQLException{
		Statement statement = connection.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		ResultSet rs = statement.executeQuery(sqlQuery);
		ResultSetMetaData rsmd = rs.getMetaData();
		
		int columns = rsmd.getColumnCount();
		ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
		
		while(rs.next())
		{
			HashMap<String, String> map = new HashMap<String, String>();
			for (int i = 1; i <= columns; i++) {
		        String value = rs.getString(i);
		        String key = rsmd.getColumnName(i);
		        map.put(key, value);
			}
			result.add(map);
		}
		return result;
	}
}