package at.campus02.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TooManyListenersException;

import at.campus02.messages.*;
import at.campus02.unoserver.Card;

/** Clientseitige Interaktionen, Verbindungsaufbau. <br>
 * Eingaben wie Username, Spielbefehle, Aufruf der Hilfefunktion, <br>
 * Erstellen des Karten-Layouts und der Handkarten ...
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 * 
 */
public class UnoClient {

/**
 * Verbindungsaufbau<br>
 * Abfrage des Namens vorgezogen, um den User zu besch&auml;&auml;ftigen und scheinbar
 * "Zeit zu sparen".<br>
 * Im Spielzug: <br>
 * <ul>
 * <li>Handkarten</li>
 * <li>Session-Score</li>
 * <li>abfangen falscher Eingaben</li>
 * <li>s&auml;mtliche m&ouml;glichen Spielz&uuml;ge</li>
 * <li>Hilfe-Funktion</li>
 * <li>etc.</li>
 * </ul>
	 * @param args -
	 */
	public static void main(String[] args) {
		try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));) {
			// System.out.println("Bitte deinen Spielernamen eingeben.");

			System.out.println("Bitte Namen eingeben.");
			String name = br.readLine();
			name = name.trim();

			try (Socket client = new Socket("localhost", 1111);
					ObjectOutputStream oos = new ObjectOutputStream(client.getOutputStream());
					ObjectInputStream ois = new ObjectInputStream(client.getInputStream());) {
				oos.writeObject(name);
				oos.flush();
				oos.reset();
				boolean keepRunning = true;

				while (keepRunning) {
					Object message = ois.readObject();

					if (message instanceof ToClientHand) {
						ToClientHand msg = (ToClientHand) message;

						System.out.println("Deine Handkarten:");
						printHandCards(msg.getHand());

						ToServerDecision dec = playCard(br);
						oos.writeObject(dec);
						oos.flush();
						oos.reset();
					} else if (message instanceof ToClientScore) {
						ArrayList<HashMap<String, String>> results = ((ToClientScore) message).getScore();
						System.out.println();
						for (HashMap<String, String> map : results) {
							System.out.println(map.get("Player") + " hat derzeit in der Session "
									+ ((ToClientScore) message).getSessionID() + ": " + map.get("Score") + " Punkte");

						}
						System.out.println();

						ToServerDecision dec = playCard(br);
						oos.writeObject(dec);
						oos.flush();
						oos.reset();

					} else if (message instanceof ToClientHistory) {
						ArrayList<HashMap<String, String>> results = ((ToClientHistory) message).getHistory();
						System.out.println();
						for (HashMap<String, String> map : results) {
							System.out.println("In Session " + map.get("Session") + " hat Player " + map.get("Player")
									+ " " + map.get("Score") + " Punkte");

						}
						System.out.println();

						ToServerDecision dec = playCard(br);
						oos.writeObject(dec);
						oos.flush();
						oos.reset();
					} else if (message instanceof ToClientCorrectCheck) {
						ToClientCorrectCheck check = (ToClientCorrectCheck) message;
						String checkMessage = check.getErrorMessage();
						boolean isCorrect = check.isCorrect();

						if (checkMessage.equalsIgnoreCase("playFalse")) {
							System.out.println(
									"Gewünschte Karte passt nicht oder Karte ist nicht in deiner Hand. Bitte um erneute Eingabe.");

							ToServerDecision dec = playCard(br);
							oos.writeObject(dec);
							oos.flush();
							oos.reset();
						} else if (checkMessage.equalsIgnoreCase("drawError")) {
							System.out.println(
									"Schöner Versuch, aber die Karte passt nicht um abgelegt zu werden. Sie wird jetzt den Handkarten hinzugefügt.");

						} else if (checkMessage.equalsIgnoreCase("nameNotExisting")) {
							System.out.println("Es gibt keinen Spieler mit dem gewünschten Namen.");

							ToServerDecision dec = playCard(br);
							oos.writeObject(dec);
							oos.flush();
							oos.reset();
						} else {

						}

					} else if (message instanceof ToClientTopCard) {

						if (((ToClientTopCard) message).getTopCard().getNumber() == 10
								|| ((ToClientTopCard) message).getTopCard().getNumber() == 11) {
							System.out.println("\rDie Farbe der offenen Karte ist: "
									+ ((ToClientTopCard) message).getTopCard().getColor() + "\r");
						} else {
							System.out.println("Offene Karte:");
						}
						printSingleCard(((ToClientTopCard) message).getTopCard());

					} else if (message instanceof ToClientColor) {
						ToClientColor col = (ToClientColor) message;
						boolean correct = col.getChooseNew();

						System.out.println("Bitte neue Farbe festlegen. Mögliche Eingaben: R, G, B, Y");
						String color = br.readLine();
						color = color.trim();
						color = color.toUpperCase();

						ToServerColor dec = new ToServerColor(color);
						oos.writeObject(dec);
						oos.flush();
						oos.reset();
					} else if (message instanceof ToClientDraw) {
						ToClientDraw card = (ToClientDraw) message;
						System.out.println("Hier deine Karte:");

						printSingleCard(card.getTopDeckCard());

						System.out.println(
								"Soll die Karte auf den Ablagestapel gelegt werden?  J / N  -  auch  Y / N  möglich");

						boolean goingOn = true;

						while (goingOn == true) {
							String decision = br.readLine();
							decision = decision.trim();

							if (decision.equalsIgnoreCase("J") || decision.equalsIgnoreCase("Y")) {
								goingOn = false;
								ToServerDraw dec = new ToServerDraw(null, true);
								oos.writeObject(dec);
								oos.flush();
								oos.reset();

							} else if (decision.equalsIgnoreCase("N")) {
								goingOn = false;
								ToServerDraw dec = new ToServerDraw(null, false);
								oos.writeObject(dec);
								oos.flush();
								oos.reset();
							} else {
								goingOn = true;
								System.out.println("Fehler bei Ja / Nein Abfrage. Bitte um erneute Entscheidung.");
							}
						}
					} else if (message instanceof ToClientTurnSerparator) {
						ToClientTurnSerparator sep = (ToClientTurnSerparator) message;

						System.out.println(sep.getSeperator());

					} else if (message instanceof Broadcast) {
						Broadcast shout = (Broadcast) message;

						if (shout.getTopCard() != null) {
							if (shout.getTopCard().getNumber() == 10 || (shout.getTopCard().getNumber() == 11)) {
								System.out.println(
										"\n" + shout.getBroadcastMessage2() + " ist jetzt am Zug. Auf der Hand werden "
												+ shout.getBroadcastMessage1() + " Karten gehalten.");
								System.out.println(
										"\rDie Farbe der offenen Karte ist: " + shout.getTopCard().getColor() + "\r");
							} else {
								System.out.println(
										"\n" + shout.getBroadcastMessage2() + " ist jetzt am Zug. Auf der Hand werden "
												+ shout.getBroadcastMessage1() + " Karten gehalten.");
								System.out.println("\rOffene Karte:\r");
							}
							printSingleCard(shout.getTopCard());
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("Rev")) {
							System.out.println("Richtung wurde geändert. Als nächstes ist "
									+ shout.getBroadcastMessage2() + " an der Reihe.");
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("skp")) {
							System.out.println(shout.getBroadcastMessage2() + " wird ausgelassen.");
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("wld")) {
							System.out.println("Neue Farbe: " + shout.getBroadcastMessage2());
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("+4")) {
							System.out.println(shout.getBroadcastMessage2() + " bekommt 4 Karten");
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("+2")) {
							System.out.println(shout.getBroadcastMessage2() + " bekommt 2 Karten");
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("playDrawn")) {
							System.out.println("Hebt eine Karte und spielt diese aus: " + shout.getBroadcastMessage2());
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("keepDrawn")) {
							System.out.println("Hebt eine Karte und behält sie auf der Hand.");
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("playing")) {
							System.out.println("Spielt: " + shout.getBroadcastMessage2() + "\r\n");
						} else if (shout.getBroadcastMessage1().equalsIgnoreCase("shoutUno")) {
							System.out.println("                                                       .---.");
							System.out.println("                                                      /  .  \\");
							System.out.println("                                                     |\\_/|   |");
							System.out.println("                                                     |   |  /|");
							System.out.println("  .--------------------------------------------------------' |");
							System.out.println(" /  .-.                                                      |");
							System.out.println("|  /   \\     Ruft:    U  U   NN   N    OOO                   |");
							System.out.println("| |\\_.  |             U  U   N N  N   O   O                  |");
							System.out.println("|\\|  | /|             U  U   N  N N   O   O                  |");
							System.out.println("| `---' |              UU    N   NN    OOO                   |");
							System.out.println("|       |                                                   /");
							System.out.println("|       |--------------------------------------------------'");
							System.out.println("\\       |");
							System.out.println(" \\     /");
							System.out.println("  `---'");

							System.out.println("\r\n\r\nund spielt: " + shout.getBroadcastMessage2());
						}

					} else if (message instanceof ToClientEndOfGame) {
						ToClientEndOfGame end = (ToClientEndOfGame) message;

						System.out.println(
								"\r\n♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪\r\n");
						System.out.println("♪ღ♪\tDas Spiel ist zu Ende. Gewonnen hat: " + end.getWinner()
								+ " und bekommt " + end.getPoints() + " Punkte");
						System.out.println(
								"\r\n♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪*•.¸¸¸.•*¨¨*•.¸¸¸.•*♪ღ♪\r\n");

					} else if (message instanceof ToClientEndOfCurrentSession) {
						ArrayList<HashMap<String, String>> results = ((ToClientEndOfCurrentSession) message)
								.getScores();
						System.out.println();
						for (HashMap<String, String> map : results) {
							System.out.println(map.get("Player") + " hat derzeit in der Session "
									+ ((ToClientEndOfCurrentSession) message).getID() + ": " + map.get("Score")
									+ " Punkte");

						}
						System.out.println();
						boolean keepON = true;

						while (keepON) {

							System.out.println("Noch eine Runde? ^^		Y/N oder J/N  als Eingabe möglich");
							String dec = br.readLine();
							dec = dec.trim();

							if (dec.equalsIgnoreCase("Y") || dec.equalsIgnoreCase("J")) {
								ToServerRematch rematch = new ToServerRematch(true);
								oos.writeObject(rematch);
								oos.flush();
								oos.reset();

								keepRunning = true;
								keepON = false;
							} else if (dec.equalsIgnoreCase("N")) {
								ToServerRematch rematch = new ToServerRematch(false);
								oos.writeObject(rematch);
								oos.flush();
								oos.reset();

								keepON = false;
								keepRunning = false;
								System.out
										.println("Dann wünschen wir noch einen schönen Tag. Bis zum nächsten Mal. :-D");
							} else {
								keepON = true;
								System.out.println("Ungültige Eingabe. Bitte erneut eine Entscheidung eingeben.");
							}
						}
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Abfrage von Spielerentscheidungen
	 * @param br AusleseReader
	 * @return Aussage, ob Spielerentscheidung g&uuml;ltig ist
	 * @throws IOException wenn garnix mehr geht
	 */
	private static ToServerDecision playCard(BufferedReader br) throws IOException {

		String command = "";
		String wantedCardOrPlayer = null;
		boolean goingOn = true;
		while (goingOn) {
			System.out.println("\r\nBitte um Aktion. (play, draw, uno, help, score oder history sind möglich)");
			command = br.readLine();
			command = command.trim();

			if (command.equalsIgnoreCase("play")) {
				goingOn = false;
				System.out.println("Welche Karte soll gespielt werden?");
				wantedCardOrPlayer = br.readLine();
				wantedCardOrPlayer = wantedCardOrPlayer.trim();
			} else if (command.equalsIgnoreCase("draw")) {
				goingOn = false;

			} else if (command.equalsIgnoreCase("uno")) {
				goingOn = false;
				System.out.println("Welche Karte soll gespielt werden?");
				wantedCardOrPlayer = br.readLine();
				wantedCardOrPlayer = wantedCardOrPlayer.trim();
			} else if (command.equalsIgnoreCase("help")) {
				getHelp();
				goingOn = true;
			} else if (command.equalsIgnoreCase("score")) {

				goingOn = false;
			} else if (command.equalsIgnoreCase("history")) {
				System.out.println(
						"Bitte um Namen des Spielers für dessen Score-History.\r\n  Name muss exakt so eingegeben werden wie er ausgegeben wird (Groß / Kleinschreibung beachten. Name kann auch von der Console kopiert werden.).");
				String name = br.readLine();
				name = name.trim();

				wantedCardOrPlayer = name;

				goingOn = false;
			} else {
				goingOn = true;
				System.out.println("Ungültige Entscheidung, bitte um erneute Entscheidung.");
			}
		}

		ToServerDecision dec = new ToServerDecision(command, wantedCardOrPlayer);
		return dec;
	}

	/**
	 * Hilfe-Funktion:<br>
	 * Gibt auf der Konsole der f&uuml;r den Spieler m&ouml;glichen Befehle und eine
	 * Kurzanleitung aus.
	 */
	private static void getHelp() {
		System.out.print("\nWillkommen bei UNO\r\n\r\n"
				+ "Das Ziel ist es, vor allen Anderen alle Handkarten auszuspielen.\r\n"
				+ "Gespielt werden kann nur eine auf die offene Karte passende Karte.\r\n"
				+ "Eine Karte ist passend, wenn die Farbe (Y, G, B, R und/oder die Zahl (1-9),\r\noder die Aktion (Wld, WldDr, Skp, Rev) übereinstimmen.\r\n"
				+ "\r\nWeitere Bedingungen:\r\n\r\n" + "* Eine Eingabe wird mit der Return-Taste abgeschlossen.\r\n"
				+ "* Um eine Karte auszuspielen, Befehl 'play' eingegeben, Return-Taste,\r\n"
				+ "  dann die Kartenbezeichnung (z.B. 'Y 6') und dann wieder Return-Taste!\r\n"
				+ "* Die Eingabe der gewählten Karte muss exakt der Kartenbezeichnung entsprechen (auch Leerzeichen), z.B: 'R 1'.\r\n"
				+ "* Eine WldDr (Farbwechsel +4) kann nicht auf eine Wld (Farbwechsel) oder umgekehrt. \r\n"
				+ "* Wenn keine passende Karte auf der Hand ist, welche abgelegt werden kann, muss eine gezogen werden.\r\n"
				+ "* Während man die vorletzte Karte ausspielt muss UNO gerufen werden. ACHTUNG! Anstelle des Befehls 'play'\r\n"
				+ "  wird der Befehl 'uno' gewählt und danach die Kartenbezeichnung eingegeben, z.B: 'uno'  \r\n"
				+ "\r\nBefehle \r\n\r\n" + "* play (spielen)\r\n" + "* draw (Karte ziehen)\r\n"
				+ "* help (diese Hilfe aufrufen)\r\n" + "* uno (uno rufen)\r\n" + "* score (aktueller Spielstand)\r\n"
				+ "* history (meine Punktehistorie)\r\n\r\n" + "");
	}

	/**
	 * Druckt das Karten-Layout
	 * @param toPrint auszugebende Karte
	 */
	
	private static void printSingleCard(Card toPrint) {

		System.out.println("\t  xxxxxxxxx ");
		System.out.println("\t  x       x ");
		System.out.printf("\t  x %s x \n", toPrint.toString());
		System.out.println("\t  x       x ");
		System.out.println("\t  xxxxxxxxx \n");

	}

	/**
	 * Erstellt die Handkarten
	 * @param handArray Handkarten
	 */
	public static void printHandCards(ArrayList<Card> handArray) {
		for (int i = 0; i < handArray.size() * 5; i++) {
			if (i == 0 || i == 4) {

				System.out.println();

				for (int ctr = 0; ctr < handArray.size(); ctr++) {
					System.out.print("xxxxxxxxx ");
				}
			} else if (i == 1 || i == 3) {
				System.out.println();
				for (int ctr = 0; ctr < handArray.size(); ctr++) {
					System.out.print("x       x ");
				}
			} else if (i == 2) {
				System.out.println(); 
				for (int ctr = 0; ctr < handArray.size(); ctr++) {
					System.out.printf("x %s x ", handArray.get(ctr).toString());
				}
			}
		}
		System.out.println();
	}

}
