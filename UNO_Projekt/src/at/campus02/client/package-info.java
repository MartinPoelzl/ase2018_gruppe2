/**Enth&auml;lt die <em>Client-Klasse</em><br>
 * Komplette Client-Server Kommunikation (Verbindung, Name, Score, etc.)
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
package at.campus02.client;