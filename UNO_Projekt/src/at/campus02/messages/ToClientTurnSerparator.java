package at.campus02.messages;

import java.io.Serializable;


/**Trennzeichen / Zierleiste zwischen Spielzügen
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientTurnSerparator implements Serializable
{

	private static final long serialVersionUID = -7081130930379102981L;
	
	private String seperator;
	
	public ToClientTurnSerparator(String seperator)
	{
		this.seperator = seperator;
		
	}
	
	
	public String getSeperator()
	{
		return seperator;
	}
	
}
