package at.campus02.messages;

import java.io.Serializable;

import at.campus02.unoserver.Card;

/**
 * Kommunikation an ALLE Spieler:<br>
 * <ul><li>z.B. Was ist die oberste Karte?</li>
 * <li>Wurde die Farbe gewechselt?</li>
 * <li>Wurde jemand ausgelassen?</li>
 * <li>etc.</li>
 *</ul>
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
public class Broadcast implements Serializable {

	private static final long serialVersionUID = 7992485671489064378L;
	private String broadcastMessage1;
	private String broadcastMessage2;
	private Card topCard;


	public Broadcast(String broadcastMessage1) {
		this.broadcastMessage1 = broadcastMessage1;
	}

	public Broadcast(Card topCard, String broadcastMessage1, String broadcastMessage2) {
		this.topCard = topCard;
		this.broadcastMessage1 = broadcastMessage1;
		this.broadcastMessage2 = broadcastMessage2;
	}

	public Broadcast(String broadcastMessage1, String broadcastMessage2) {
		this.broadcastMessage1 = broadcastMessage1;
		this.broadcastMessage2 = broadcastMessage2;
	}

	public String getBroadcastMessage1() {
		return broadcastMessage1;
	}

	public String getBroadcastMessage2() {
		return broadcastMessage2;
	}

	public Card getTopCard() {
		return topCard;
	}

}
