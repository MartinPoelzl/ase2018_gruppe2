package at.campus02.messages;

import java.io.Serializable;


/**Noch ein Spiel
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToServerRematch implements Serializable
{
	private static final long serialVersionUID = -8393471163581919994L;
	
	private boolean rematch;
	
	public ToServerRematch(boolean rematch)
	{
		this.rematch = rematch;
	}
	
	public boolean getRematch()
	{
		return rematch;
	}

}
