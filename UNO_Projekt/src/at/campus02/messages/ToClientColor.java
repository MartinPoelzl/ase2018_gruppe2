package at.campus02.messages;

import java.io.Serializable;

/**Farbwahl
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientColor implements Serializable
{
	private static final long serialVersionUID = -7603909878165865886L;
	private boolean chooseNew;
	
	public ToClientColor()
	{
		
	}
	
	public ToClientColor(boolean chooseNew)
	{
		this.chooseNew = chooseNew;
	}
	
	public boolean getChooseNew()
	{
		return chooseNew;
	}
	
	public void setChooseNew(boolean chooseNew)
	{
		this.chooseNew = chooseNew;
	}
	
}
