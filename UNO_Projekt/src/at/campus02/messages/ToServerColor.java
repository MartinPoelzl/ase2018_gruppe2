package at.campus02.messages;

import java.io.Serializable;


/**Gew&auml;hlte Farbe
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToServerColor implements Serializable
{

	private static final long serialVersionUID = 190751845738693245L;
	private String color;
	
	public ToServerColor(String color)
	{
		this.color = color;
	}
	
	public String getColor()
	{
		return color;
	}
	
}
