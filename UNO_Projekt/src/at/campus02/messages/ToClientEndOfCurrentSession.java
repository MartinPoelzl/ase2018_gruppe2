package at.campus02.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**Ende der aktuellen Session
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientEndOfCurrentSession implements Serializable
{

	private static final long serialVersionUID = -2964569681296325402L;
	
	private boolean endOfSession;
	private ArrayList<HashMap<String, String>> scores;
	private int sessionID;
	
	public ToClientEndOfCurrentSession(boolean endOfSession, ArrayList<HashMap<String, String>> scores, int sessionID)
	{
		this.endOfSession = endOfSession;
		this.scores = scores;
		this.sessionID = sessionID;
	}
	
	public boolean getEndOfSession()
	{
		return endOfSession;
	}
	public ArrayList<HashMap<String, String>> getScores()
	{
		return scores;
	}
	
	public int getID()
	{
		return sessionID;
	}
	
}
