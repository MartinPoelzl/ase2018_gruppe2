package at.campus02.messages;

import java.io.Serializable;

import at.campus02.unoserver.Card;


/**Offene Karte
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientTopCard implements Serializable
{

	private static final long serialVersionUID = 1079979628906460274L;
	
	private Card topCard;
	
	public ToClientTopCard(Card topCard)
	{
		this.topCard = topCard;
	}

	public Card getTopCard()
	{
		return topCard;
	}
	
	
	

}
