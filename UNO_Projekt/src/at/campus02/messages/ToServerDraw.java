package at.campus02.messages;

import java.io.Serializable;


/**Returniert die Antwort auf die Entscheidung "Ablegen oder auf der Hand behalten?"
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToServerDraw implements Serializable
{
	
	private static final long serialVersionUID = 3406301214145322809L;
	private String draw;
	private boolean toPlay;
	
	public ToServerDraw(String draw, boolean toPlay)
	{
		this.draw = draw;
		this.toPlay = toPlay;
		
	}

	public String getDraw() {
		return draw;
	}

	public boolean isToPlay() {
		return toPlay;
	}
	
	
	
	
}
