package at.campus02.messages;

import java.io.Serializable;


/**Spielerentscheidung
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToServerDecision implements Serializable
{
	private static final long serialVersionUID = -7132090951675481661L;
	
	private String decision;
	private String wantedCardOrPlayer;
	
	
	public ToServerDecision(String decision, String wantedCardOrPlayer)
	{
		this.decision = decision;
		this.wantedCardOrPlayer = wantedCardOrPlayer;
	}
	
	
	public String getDecision()
	{
		return decision;
	}
	
	public String getWantedCard()
	{
		return wantedCardOrPlayer;
	}
	
}
