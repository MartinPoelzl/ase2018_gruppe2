/**Enth&auml;lt <em>s&auml;mtliche Arten von Nachrichten</em> zwischen Client und Server
 * 
 * @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
 *         Strau&szlig;<br>
 * @version 1.0, Projektarbeit SS2018 - FH campus02
 *
 */
package at.campus02.messages;