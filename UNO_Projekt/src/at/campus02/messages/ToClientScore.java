package at.campus02.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;


/**Punktestand
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientScore implements Serializable
{

	private static final long serialVersionUID = 2258206415887002049L;
	
	private ArrayList<HashMap<String, String>> score;
	private int sessionID;
	
	public ToClientScore(ArrayList<HashMap<String, String>> score, int sessionID)
	{
		this.score = score;
		this.sessionID = sessionID;
	}

	public ArrayList<HashMap<String, String>> getScore()
	{
		return score;
	}

	public int getSessionID()
	{
		return sessionID;
	}
	
	

}
