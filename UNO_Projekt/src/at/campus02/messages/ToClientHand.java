package at.campus02.messages;

import java.io.Serializable;
import java.util.ArrayList;

import at.campus02.unoserver.Card;

/**Handkarten
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientHand implements Serializable
{	
	
	// enum mitschicken (turn, name, db, UNO SAGEN!!! spieler soundso ist am zug
	// (broadcast)...
	// aha - das ist ein spielername
	// ruf die funktion auf, die spielernamen verwalten kann
	// schickt das ganze an den server
	private String action;
	private String message;
	
	private static final long serialVersionUID = -2816727627689624817L;
	
	private ArrayList<Card> hand;
	
	
	
	// Client bittet um spielernamen
	// client schickt spielernamen
	// Client bekommt die Handkarten und zeigt diese an
	// Aufforderung zur Interaktion
	// Client schickt Spielerentscheidung an Server
	// Client empf&auml;ngt reaktion von Server
	// Client bekommt Ergebnisse aus DB

	public ToClientHand(ArrayList<Card> hand) 
	{
		this.hand = hand;
	}
	

	public ArrayList<Card> getHand()
	{
		return hand;
	}


}
