package at.campus02.messages;

import java.io.Serializable;
/**Falsche Eingabe
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientCorrectCheck implements Serializable
{

	private static final long serialVersionUID = 4713309461445734701L;
	
	private String errorMessage;
	private boolean correct;
	
	public ToClientCorrectCheck(String errorMessage, boolean correct)
	{
		this.errorMessage = errorMessage;
		this.correct = correct;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public boolean isCorrect()
	{
		return correct;
	}
	
	
	

}
