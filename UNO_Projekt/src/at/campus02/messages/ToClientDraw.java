package at.campus02.messages;

import java.io.Serializable;

import at.campus02.unoserver.Card;

/**Soll die gezogene Karte auf der Hand behalten, oder abgelegt werden?
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientDraw implements Serializable
{
	
	
	private static final long serialVersionUID = -2018443911334684456L;
	private Card topDeckCard;
	
	public ToClientDraw(Card topDeckCard)
	{
		this.topDeckCard = topDeckCard;
		
	}

	public Card getTopDeckCard()
	{
		return topDeckCard;
	}
	
	
}
