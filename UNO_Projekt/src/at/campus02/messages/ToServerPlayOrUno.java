package at.campus02.messages;

import java.io.Serializable;


/**Uno gerufen
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToServerPlayOrUno implements Serializable
{
	private static final long serialVersionUID = 2539492770317818920L;
	private String command;
	private String wantedCard;

	public ToServerPlayOrUno(String command, String wantedCard)
	{
		this.command = command;
		this.wantedCard = wantedCard;
	}

	public String getCommand()
	{
		return command;
	}

	public String getWantedCard()
	{
		return wantedCard;
	}
	
	
}
