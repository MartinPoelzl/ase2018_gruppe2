package at.campus02.messages;

import java.io.Serializable;

/**Ende des Spieles
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientEndOfGame implements Serializable
{
	private static final long serialVersionUID = 6191147757317955243L;
	private boolean endOfGame;
	private String winner;
	private String pointsAddedToWinner;
	
	public ToClientEndOfGame(boolean endOfGame, String winner, String pointsAddedToWinner)
	{
		this.winner = winner;
		this.endOfGame = endOfGame;		
		this.pointsAddedToWinner = pointsAddedToWinner;
	}
	
	public boolean getEndOfGame()
	{
		return endOfGame;
	}
	
	public String getWinner()
	{
		return winner;
	}
	
	public String getPoints()
	{
		return pointsAddedToWinner;
	}
}
