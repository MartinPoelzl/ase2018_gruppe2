package at.campus02.messages;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**Spielst&auml;nde
*
* @author Sabrina Gerger, Erzseb&eacute;th N&eacute;meth, Amelia P&ouml;cheim, Veronika
*         Strau&szlig;<br>
* @version 1.0, Projektarbeit SS2018 - FH campus02
*
*/
public class ToClientHistory implements Serializable
{

	private static final long serialVersionUID = -2436987262411276906L;
	
	private ArrayList<HashMap<String, String>> history;
	private int sessionID;
	
	public ToClientHistory(ArrayList<HashMap<String, String>> history, int sessionID)
	{
		this.history = history;
		this.sessionID = sessionID;
	}

	public ArrayList<HashMap<String, String>> getHistory()
	{
		return history;
	}
	
	public int getSessionID()
	{
		return sessionID;
	}
}
